import redis


def main():
    redis_pool = redis.ConnectionPool(host='redis', port='6379', db=0)
    redis_session = redis.StrictRedis(connection_pool=redis_pool)

    try:
        locks = redis_session.keys("OpsPushUserExtraction.*.estimate.lock")
    except redis.exceptions.ConnectionError:
        print("Error: redis.exceptions.ConnectionError")

    if locks:
        for key in locks:
            redis_session.delete(key)
            print("sidecar >> redis >> {} deleted.".format(
                key))
    else:
        print("sidecar >> redis >> No lock exist.")
    print("sidecar >> finished.")
    return 0


if __name__ == '__main__':
    main()
