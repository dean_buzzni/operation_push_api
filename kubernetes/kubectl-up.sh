#!/usr/bin/env bash
# Create kubecernets namespace
kubectl create -f namespaces-opspush.yaml

# Create ImagePullSecret
kubectl create -f secrets-regbuzzni.yaml

# Create kubernetes services
kubectl create -f services-rabbitmq.yaml
kubectl create -f services-postgres.yaml
kubectl create -f services-interface.yaml
kubectl create -f services-redis.yaml

# Create kubernetes ingress
kubectl create -f ingress-interface.yaml

# Create kubernetes StatefulSet
kubectl create -f statefulset-redis.yaml
kubectl create -f statefulset-postgres.yaml
kubectl create -f statefulset-rabbitmq.yaml

# Create kubernetes deployments
kubectl create -f deployments-spawn-worker.yaml
kubectl create -f deployments-ready-worker.yaml
kubectl create -f deployments-finish-worker.yaml
kubectl create -f deployments-resume-worker.yaml
kubectl create -f deployments-test-worker.yaml
kubectl create -f deployments-interface.yaml

# Create kubernetes cron jobs
kubectl create -f cronjobs-expired-collector.yaml
