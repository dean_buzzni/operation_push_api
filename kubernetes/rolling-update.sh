
kubectl set image deployment/interface -n opspush-backend interface=reg.buzzni.net/marco/push-interface:latest

kubectl set image deployment/finish-worker -n opspush-backend finish-worker=reg.buzzni.net/marco/push-finish-worker:latest
kubectl set image deployment/ready-worker -n opspush-backend ready-worker=reg.buzzni.net/marco/push-ready-worker:latest
kubectl set image deployment/resume-worker -n opspush-backend resume-worker=reg.buzzni.net/marco/push-resume-worker:latest
kubectl set image deployment/spawn-worker -n opspush-backend spawn-worker=reg.buzzni.net/marco/push-spawn-worker:latest
kubectl set image deployment/test-worker -n opspush-backend test-worker=reg.buzzni.net/marco/push-test-worker:latest
