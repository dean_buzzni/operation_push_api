import requests
import pickle

from datetime import datetime
from pytz import timezone
from sources.database import Base
from sources.database import shopping_db_session
from sources.database import redis_session
# from database import shopping_db_session, Base, redis_session

from sqlalchemy import BigInteger
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import SmallInteger
from sqlalchemy import Text


# Shopping slave DB.user_device
class UserDeviceModel(Base):
    """
    Shopping slave DB.user_device Table의 abstract

    push token을 추출하기 위해 필요한 column만 가져옴
    """
    __tablename__ = 'user_device'
    id = Column(BigInteger, primary_key=True)
    user_id = Column(Text)
    device_id = Column(BigInteger)
    update_date = Column(BigInteger)
    push_code = Column(Text)
    app_version = Column(Text)
    status = Column(SmallInteger)


# Shopping slave DB.user
class UserModel(Base):
    """
    Shopping slave DB.user_device Table의 abstract

    push token을 추출하기 위해 필요한 column만 가져옴
    """
    __tablename__ = 'user'
    id = Column(BigInteger, primary_key=True)
    is_event_push = Column(Boolean)


# Shopping slave DB.device
class DeviceModel(Base):
    """
    Shopping slave DB.device Table의 abstract

    push token을 추출하기 위해 필요한 column만 가져옴
    """
    __tablename__ = 'device'
    id = Column(BigInteger, primary_key=True)
    code = Column(Text)
    os = Column(Text)


# Shopping slave DB.visit
class VisitModel(Base):
    """
    Shopping slave DB.visit Table의 abstract

    push token을 추출하기 위해 필요한 column만 가져옴
    """
    __tablename__ = 'visit'
    id = Column(Text, primary_key=True)
    user_id = Column(Text)
    action_id = Column(BigInteger)
    action = Column(Text)
    create_date = Column(BigInteger)


def get_userset_who_deny_event_push(count=False):
    """
    Event Push 받기를 거절한 사람 목록을 반환하는 함수

    shoppingDB.User.is_event_push == False인 user_id List

    issue#2

    Shopping DB 참조 속도 문제때문에 Cache를 참조하는 API에서

    데이터를 가져오기로 함 (@jacob과 작업)

    url = http://rpc.hsmoa.com/user/getEventPushExcludeList?xid=6I9TUXKrqt

    :param bool count: default=False, True면 user_set 대신 (int)count 반환

    :return: (set) 이벤트 푸시 받기를 거절한 모든 유저의 set
    """
    print('get_userset_who_deny_event_push()')

    start = datetime.now()
    r = requests.get(
        'http://rpc.hsmoa.com/user/getEventPushExcludeList?xid=6I9TUXKrqt'
    )
    end = datetime.now()
    payload = set()
    if r.json()['success'] is False:
        print("Cache Error, Access to DB")
        start = datetime.now()
        s = shopping_db_session
        try:
            query_result = s.query(UserModel.id).filter(
                UserModel.is_event_push is False
            ).all()
        except Exception:
            s.rollback()
            raise
        finally:
            s.close()
        end = datetime.now()
        for x in query_result:
            payload.add(x[0])
    else:
        payload = set(r.json()['result']['data'])

    print("GET deny: {}".format(end - start))

    if count:
        return len(payload)
    else:
        return payload


def ordinary_user_extraction(day_interval, date_offset=None, count=False):
    """
    A ~ B 기간에 접속한 유저들 중에서 이벤트 푸시받기를 허용한 사람들의 user_id set를 반환.

    만약 count == True이면 set의 크기를 반환함

    :param datetime.timedelta day_interval: (int) date_offset로 부터 몇일 전까지 접속한 유저를 찾을까?
    :param datetime.datetime date_offset: 서비스에 접속한 유저를 찾기위한 기준 일 (None=지금)
    :param bool count: default=False, True면 user_set 대신 (int)count 반환

    :return: set or int
    """
    print('ordinary_user_extraction()')
    start = datetime.now()

    # 기간 내에 접속한 android user
    # {(user_id, last_login, token), ...}
    android_user_set = get_android_userset_with_day_interval(
        day_interval=day_interval,
        date_offset=date_offset
    )

    # 기간 내에 접속한 ios user
    # {(user_id, last_login, token), ...}
    ios_user_set = get_ios_userset_with_day_interval(
        day_interval=day_interval,
        date_offset=date_offset
    )
    end = datetime.now()
    print("extract : {}".format(end - start))

    # 이벤트 푸시 받기를 거절한 유저 set을 받아옴
    deny_push_user_set = get_userset_who_deny_event_push()
    start = datetime.now()
    allowed_android_user_set = set()
    allowed_ios_user_set = set()

    # android_user_set, ios_user_set 중 푸시 거절한 유저 제외
    for x in android_user_set:
        if x[0] not in deny_push_user_set:
            allowed_android_user_set.add(x)
    for x in ios_user_set:
        if x[0] not in deny_push_user_set:
            allowed_ios_user_set.add(x)

    end = datetime.now()
    print("denying : {}".format(end - start))

    # Multi device 처리
    r = multi_device_classifier(android_set=android_user_set,
                                ios_set=ios_user_set)

    if count:
        return len(r['android']) + len(r['ios'])
    else:
        return {'android': r['android'], 'ios': r['ios']}


def get_android_userset_with_day_interval(day_interval, date_offset=None,
                                          count=False):
    """
    offset부터 offset+day_interval 사이 기간동안 서비스를 이용한 android user의 user_id를 추출한다.

    :param datetime.timedelta day_interval: (int) date_offset로 부터 몇일 전까지 접속한 유저를 찾을까?
    :param datetime.datetime date_offset: 서비스에 접속한 유저를 찾기위한 기준 일 (None=지금)
    :param bool count: default=False, True면 user_set 대신 (int)count 반

    :return: set or int
    """
    interval_delta = day_interval
    date_offset = datetime.now(tz=timezone('Asia/Seoul'))

    if date_offset:
        date_offset = date_offset

    date_range_end = date_offset
    date_range_start = date_range_end + interval_delta

    numerical_start = int(date_range_start.strftime('%Y%m%d%H%M%S'))
    numerical_end = int(date_range_end.strftime('%Y%m%d%H%M%S'))

    s = shopping_db_session
    try:
        query_result = s.query(UserDeviceModel.user_id,
                               UserDeviceModel.update_date,
                               UserDeviceModel.push_code
                               )
        query_result = query_result.filter(
            UserDeviceModel.update_date >= numerical_start,
            UserDeviceModel.update_date <= numerical_end,
            UserDeviceModel.app_version != '0',
            UserDeviceModel.status >= 0,
            UserDeviceModel.push_code != ''
        )
        query_result = query_result.all()
    except Exception:
        s.rollback()
        raise
    finally:
        s.close()

    result_set = set()
    for x in query_result:
        result_set.add((x.user_id, x.update_date, x.push_code))

    if count:
        return len(result_set)
    else:
        return result_set


def get_ios_userset_with_day_interval(day_interval, date_offset=None,
                                      count=False):
    """
    offset부터 offset+day_interval 사이 기간동안 서비스를 이용한 ios user의 user_id를 추출한다.

    :param datetime.timedelta day_interval: (int) date_offset로 부터 몇일 전까지 접속한 유저를 찾을까?
    :param datetime.datetime date_offset: 서비스에 접속한 유저를 찾기위한 기준 일 (None=지금)
    :param bool count: default=False, True면 user_set 대신 (int)count 반

    :return: set or int
    """
    interval_delta = day_interval
    date_offset = datetime.now(tz=timezone('Asia/Seoul'))

    if date_offset:
        date_offset = date_offset

    date_range_end = date_offset
    date_range_start = date_range_end + interval_delta

    numerical_start = int(date_range_start.strftime('%Y%m%d%H%M%S'))
    numerical_end = int(date_range_end.strftime('%Y%m%d%H%M%S'))

    s = shopping_db_session
    try:
        query_result = s.query(UserDeviceModel.user_id,
                               UserDeviceModel.update_date,
                               DeviceModel.code
                               )
        query_result = query_result.filter(
            DeviceModel.os.in_(['IPhone', 'IPad']),
            DeviceModel.code != '000000000000000000000000000000000000000000000'
            '0000000000000000000',
            DeviceModel.id == UserDeviceModel.device_id,
            UserDeviceModel.update_date >= numerical_start,
            UserDeviceModel.update_date <= numerical_end,
        )
        query_result = query_result.all()
    except Exception:
        s.rollback()
        raise
    finally:
        s.close()

    result_set = set()
    for x in query_result:
        result_set.add((x.user_id, x.update_date, x.code))

    if count:
        return len(result_set)
    else:
        return result_set


def userid_to_token_extraction(userid_set):
    """
    user_id set을 받아서 multi device 처리 하고

    user_id에 해당하는 push token set 반환

    :param set userid_set: push tokenize 되기 전의 user_id set
    :return: user_id를 push_token으로 변환한 set
    {'android': android_userset, 'ios': ios_userset}
    """
    s = shopping_db_session
    print('userid_to_token_extraction()')
    start = datetime.now()
    # Android user
    try:
        android_set = s.query(UserDeviceModel.user_id,
                              UserDeviceModel.update_date,
                              UserDeviceModel.push_code
                              ).filter(
            UserDeviceModel.user_id.in_(userid_set),
            UserDeviceModel.app_version != '0',
            UserDeviceModel.status >= 0,
            UserDeviceModel.push_code != ''
        ).all()
    except Exception:
        s.rollback()
        s.close()
        raise

    # iOS user
    try:
        ios_set = s.query(UserDeviceModel.user_id,
                          UserDeviceModel.update_date,
                          DeviceModel.code
                          ).filter(
            UserDeviceModel.user_id.in_(userid_set),
            DeviceModel.os.in_(['IPhone', 'IPad']),
            DeviceModel.code != '000000000000000000000000000000000000000000000'
            '0000000000000000000',
            DeviceModel.id == UserDeviceModel.device_id
        ).all()
    except Exception:
        s.rollback()
        raise
    finally:
        s.close()

    end = datetime.now()
    print("tokenize : {}".format(end - start))

    # Android + iOS multi_device_users
    r = multi_device_classifier(android_set=android_set,
                                ios_set=ios_set)
    return {'android': r['android'], 'ios': r['ios']}


def csv_user_extraction(redis_csvfile_key):
    """
    csv에서 가져온 user_id중 푸시 받기 거절한 user_id를 제외하고,

    유저중 가장 최근에 접속한 device의 push_token을 맵핑한다.

    :param str redis_csvfile_key: 유저가 업로드한 csv file의 hash key

    :return: push token set
    { 'android': {(user_id, token), ... },
      'ios' : { (user_id, token), ... }}
    """
    print('csv_user_extraction()')
    start = datetime.now()
    # 1. redis 에서 pickled 가져오기
    pickled = redis_session.get(redis_csvfile_key)

    # 2. pickled 파싱하기
    unpickled = pickle.loads(pickled)
    csv_user_set = set()
    column_names = "num,xid"
    splited_stream = unpickled.split('\n')

    if splited_stream[0] != column_names:
        print("invalid CSV")
    else:
        for line in splited_stream[1:]:
            if line == "":
                break
            else:
                user_id = line.split(',')[1]
                csv_user_set.add(user_id)

    end = datetime.now()
    print("extract: {}".format(end - start))

    # 3. 이벤트 푸시 받기 거절한 유저 set 가져오기
    start = datetime.now()
    users_who_deny_event_push = get_userset_who_deny_event_push()

    # 4. 이벤트 푸시 받기 거절한 유저 제외하기
    allowed_csv_user_set = csv_user_set - users_who_deny_event_push
    end = datetime.now()
    print("denying: {}".format(end - start))

    # 5. return user_id_to_token_extraction()하기
    return userid_to_token_extraction(allowed_csv_user_set)


def test_user_extraction(test_users):
    """
    OpsPushTestReceiver에 등록된 user_id 중

    event_push_deny, multi_device_classification을 통과한 user_id의
    push_token을 추출함.

    :param dict test_users: OpsPushTestReceiver에 등록된 user_id set
    :return: push_tokens
    """
    # 이벤트 푸시 받기를 거절한 유저 set을 받아옴
    return userid_to_token_extraction(test_users)


def keywords_user_extraction(keywords):
    """
    (Deprecated)

    input keywords에 대한 user_set을 추천 API로부터 가져온다.

    user_id 선택 알고리즘은 추천 API에서 처리함.

    각 키워드에 해당하는 user_id를 받아 축적한다.

    user_id중 푸시 받기 거절한 user_id를 제외하고,

    가장 최근에 접속한 device의 push token을 찾아 맵핑한다.

    :param str keyword: query string에 포함할 keyword

    :return: (dict) push token
    { 'android' : {(user_id, token), ...},
      'ios' : {(user_id, token), ... }}
    """

    print('keywords_user_extraction()')

    keyword_user_set = set()
    # Keyword 마다 user_set 받아오기
    # example http://rpc.hsmoa.com/personalized_contents/push_recommend/믹서기
    RECOMMEND_API = "http://rpc.hsmoa.com/personalized_contents" +\
        "/push_recommend/"
    for word in keywords:
        r = requests.get(url=RECOMMEND_API + word).json()
        if r['success'] is True:
            for x in r['result']['user_id_list']:
                keyword_user_set.add(x)

    # Push 받기 거절한 user 제외
    users_who_deny_event_push = get_userset_who_deny_event_push()
    allowed_user_set = keyword_user_set - users_who_deny_event_push

    print(len(allowed_user_set))

    # multi device 처리
    return userid_to_token_extraction(allowed_user_set)


def multi_device_classifier(android_set, ios_set):
    """
    한개의 user_id가 여러 device에서 사용되고 있을 때,
    마지막으로 접속한 device로 push를 발송하기 위해 사용하는 method.

    :param set android_set: {(user_id, last_login, push_token), ... }
    :param set ios_set: {(user_id, last_login, push_token), ... }

    :return: dict {'android': result_android_set, 'ios': result_ios_set}
    """
    print("multi_device_classifier()")
    start = datetime.now()

    # 한개의 user_id가 여러 device에서 사용되고 있을 때, 마지막으로 접속한 device token 찾기
    multi_device_users = dict()
    for x in ios_set:
        if x[0] in multi_device_users:
            if x[1] > multi_device_users[x[0]][2]:
                multi_device_users[x[0]] = ('ios', x[0], x[1], x[2])
        else:
            multi_device_users[x[0]] = ('ios', x[0], x[1], x[2])

    for x in android_set:
        if x[0] in multi_device_users:
            if x[1] > multi_device_users[x[0]][2]:
                multi_device_users[x[0]] = ('android', x[0], x[1], x[2])
        else:
            multi_device_users[x[0]] = ('android', x[0], x[1], x[2])

    result_android_set = set()
    result_ios_set = set()
    for key in multi_device_users:
        if multi_device_users[key][0] == 'android':
            result_android_set.add((multi_device_users[key][1],
                                    multi_device_users[key][2],
                                    multi_device_users[key][3]
                                    ))
        else:
            result_ios_set.add((multi_device_users[key][1],
                                multi_device_users[key][2],
                                multi_device_users[key][3]
                                ))

    # 1개의 device token을 여러 user_id 가 사용할 때, 마지막으로 사용한 user_id 찾기
    multi_user_ios_tokens = dict()
    multi_user_android_tokens = dict()

    for x in result_ios_set:
        if x[2] in multi_user_ios_tokens:
            if x[1] > multi_user_ios_tokens[x[2]][1]:
                multi_user_ios_tokens[x[2]] = (x[0], x[1], x[2])
        else:
            multi_user_ios_tokens[x[2]] = (x[0], x[1], x[2])
    for x in result_android_set:
        if x[2] in multi_user_android_tokens:
            if x[1] > multi_user_android_tokens[x[2]][1]:
                multi_user_android_tokens[x[2]] = (x[0], x[1], x[2])
        else:
            multi_user_android_tokens[x[2]] = (x[0], x[1], x[2])

    result_android_set = set()
    result_ios_set = set()
    for key in multi_user_ios_tokens:
        result_ios_set.add((multi_user_ios_tokens[key][0],
                            multi_user_ios_tokens[key][2]))
    for key in multi_user_android_tokens:
        result_android_set.add((multi_user_android_tokens[key][0],
                                multi_user_android_tokens[key][2]))

    end = datetime.now()
    print("classify : {}".format(end - start))
    return {'android': result_android_set, 'ios': result_ios_set}


def event_user_extraction(event_id, from_date, to_date):
    """
    OpsPushUserExtraction.type이 'event'일때 사용하는 추출 method

    외부 event API(@zunik)로 user_id list를 요청하고 이 시스템 내에서 push token으로 변환한다.

    :param str event_id: 예) tenmillion_20181008
    :param str from_date: 예) 20181010
    :param str to_date: 예) 20181011

    :return: userid_to_token_extraction(allowed_event_user_set)
    """
    print('event_user_extraction()')

    event_user_set = set()
    start = datetime.now()

    EVENT_RELAED_URL = "http://rpc.hsmoa.com/event_answer/getUserList?" + \
        "event_id={event_id}&from_date={from_date}&to_date={to_date}".format(
            event_id=event_id, from_date=from_date, to_date=to_date)

    r = requests.get(EVENT_RELAED_URL)
    end = datetime.now()

    if r.json()['result']['count'] < 0:
        event_user_set = set()
    else:
        event_user_set = set(r.json()['result']['data'])

    print("extract: {}".format(end - start))

    # 이벤트 푸시 받기 거절한 유저 set 가져오기
    start = datetime.now()
    users_who_deny_event_push = get_userset_who_deny_event_push()

    # 이벤트 푸시 받기 거절한 유저 제외하기
    allowed_event_user_set = event_user_set - users_who_deny_event_push
    end = datetime.now()
    print("denying: {}".format(end - start))

    # return user_id_to_token_extraction()하기
    return userid_to_token_extraction(allowed_event_user_set)


def main():
    # Test Code
    # get_userset_who_deny_event_push()
    # ordinary_user_extraction(day_interval=timedelta(days=-1))
    event_user_extraction(event_id='tenmillion_20181008',
                          from_date=20181010,
                          to_date=20181011)


if __name__ == '__main__':
    main()
