import redis

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker


TEST_DB = "localhost"

Base = declarative_base()

engine = create_engine('{engine}://{username}:{password}@{host}:{port}/{db_name}'.
                       format(engine='postgres',
                              username='postgres',
                              password='buzzni2018',
                              host='postgres',
                              port='5432',
                              db_name='postgres'),
                       convert_unicode=True)
Session = scoped_session(sessionmaker(bind=engine,
                                      autocommit=False,
                                      autoflush=False))

shopping_slave_engine = create_engine('{engine}://{username}:{password}@{host}:{port}/{db_name}'.
                                      format(engine='postgres',
                                             username='newmoni',
                                             password='eksrnsqjwmsl',
                                             host='shopping-slave-1.clb1ru5wfe8t.ap-northeast-1.rds.amazonaws.com',
                                             port='5432',
                                             db_name='shopping'),
                                      convert_unicode=True)

shopping_db_session = scoped_session(sessionmaker(bind=shopping_slave_engine,
                                                  autocommit=False,
                                                  autoflush=False))

redis_pool = redis.ConnectionPool(host='redis', port='6379', db=0)
redis_session = redis.StrictRedis(connection_pool=redis_pool)
