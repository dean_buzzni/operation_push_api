from datetime import datetime

from pytz import timezone

from sources.database import Session
from sources.models.ops_push import OpsPush
from sources.models.ops_push_task import OpsPushTask


def expired_collector():
    """
    push.state == 'paused' 중 expire가 지난 Push를 'failed' 처리한다.

    Kubernetes cron job으로 주기적으로 실행된다.
    """
    try:
        push_list = Session.query(OpsPush).filter(
            OpsPush.state == 'paused',
            OpsPush.expire < datetime.now(tz=timezone("Asia/Seoul"))
        ).all()
    except Exception:
        Session.close()
        return "fail", "Push.state == 'paused' 가져오는데 문제가 발생했습니다."

    if not push_list:
        Session.close()
        return "success", "Failed 처리할 push가 없습니다."

    for push in push_list:
        push.state = 'failed'

        try:
            push_tasks = Session.query(OpsPushTask).filter(
                OpsPushTask.state == 'paused',
                OpsPushTask.ops_push_id == push.id
            ).all()
        except Exception:
            continue

        if push_tasks:
            for push_task in push_tasks:
                push_task.state = 'failed'

    try:
        Session.commit()
    except Exception:
        return "fail", "Session commit에 실패했습니다."
    finally:
        Session.close()

    return "success", "{}개의 push를 failed 처리했습니다.".format(len(push_list))


if __name__ == '__main__':
    result, msg = expired_collector()
    print(result, msg)
