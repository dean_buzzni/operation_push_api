import falcon

from falcon.media.validators.jsonschema import validate

from sources.database import Session
from sources.models.ops_push_test_receiver import OpsPushTestReceiver
from sources.schemas import load_schema

from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm.exc import NoResultFound


class PushTestReceiver(object):
    """ id에 해당하는 한개의 Test Receiver 처리 """

    def on_get(self, req, resp, test_reciver_id):
        """User Extraction 을 조회한다.

        id에 해당되는 User Extration 을 조회해서 Json 형태로
        response 해준다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int test_reciver_id: OpsPushTestReceiver.id
        """
        try:
            receiver = Session.query(OpsPushTestReceiver).filter(
                OpsPushTestReceiver.id == test_reciver_id).one()
        except NoResultFound:
            raise falcon.HTTPNotFound()
        finally:
            Session.close()
        resp.media = receiver.to_dict()
        resp.status = falcon.HTTP_OK

    def on_delete(self, req, resp, test_reciver_id):
        """PushTestReceiver 을 삭제한다.

        id에 해당되는 값을 삭제한다. 성공하면 삭제해서 컨텐츠가 없다는 의미로
        NO_CONTENT 즉 '204'를 반환한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int test_reciver_id: OpsPushTestReceiver.id
        """
        try:
            receiver = Session.query(OpsPushTestReceiver).filter(
                OpsPushTestReceiver.id == test_reciver_id).one()
            Session.delete(receiver)
            Session.commit()

            resp.status = falcon.HTTP_NO_CONTENT
        except NoResultFound:
            Session.rollback()
            raise falcon.HTTPNotFound()
        finally:
            Session.close()


class PushTestReceiverCollection(object):
    """ TestReceiver Collection """

    def on_get(self, req, resp):
        """
        다수의 TestReceiver를 포함한 json Object를 반환

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        offset = req.get_param_as_int(name="offset",
                                      required=True)
        limit = req.get_param_as_int(name="limit",
                                     required=True)

        # DB
        query_result = Session.query(OpsPushTestReceiver)
        # Pagination

        try:
            query_result = query_result[offset:offset + limit]
        except InvalidRequestError:
            Session.rollback()
            raise
        finally:
            Session.close()

        payload = []
        for x in query_result:
            payload.append(x.to_dict())

        response_body = {'payload': payload, 'count': len(payload)}

        resp.media = response_body
        resp.status = falcon.HTTP_OK

    @validate(load_schema('push_test_receiver_create'))
    def on_post(self, req, resp):
        """
        PushTestReceiver Object를 DB에 추가
        json schema를 통과해야함

        성공; HTTP_CREATED
        실패; HTTP_INTERNAL_SERVER_ERROR

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        payload = req.media

        # payload To Model
        new_push_test_receiver = OpsPushTestReceiver(**payload)
        # DB

        try:
            Session.add(new_push_test_receiver)
            Session.commit()

            # HTTP REQUEST
            resp.status = falcon.HTTP_CREATED
            resp.body = '/testreceivers/{}'.format(new_push_test_receiver.id)
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()
