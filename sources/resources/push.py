import falcon
import pickle

from datetime import datetime
from datetime import timedelta
from math import ceil
from pytz import timezone

from falcon.media.validators.jsonschema import validate
from sqlalchemy import and_
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm.exc import NoResultFound

from sources.database import Session
from sources.database import redis_session
from sources.models.ops_push import OpsPush
from sources.models.ops_push_contents import OpsPushContent
from sources.models.ops_push_task import OpsPushTask
from sources.models.ops_push_user_extraction import OpsPushUserExtraction
from sources.schemas import load_schema
from sources.conf import SEND_PER_SLICE
from sources.conf import MINUTE_INTERVAL


class Push(object):
    """ 1개의 OpsPush Model에 접근하는 Class """

    def on_get(self, req, resp, push_id):
        """
        Push.id == push_id 인 OpsPush json 반환.

        성공; HTTP_OK
        실패; HTTP_NOT_FOUND

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            raise falcon.HTTPNotFound()
        finally:
            Session.close()

        contents = push.to_dict()

        # Redis에 저장된 정보 (user_set payload에 포함시키기)
        r = redis_session.get("OpsPush.{}.userset".format(push_id))
        if r:
            contents["actual_user_count"] = pickle.loads(r)['count']

        # push_id for except from sending list
        push_id_to_deny = redis_session.get(
            "OpsPush.{}.push_id_to_deny".format(push_id))
        if push_id_to_deny:
            contents['push_id_to_deny'] = pickle.loads(push_id_to_deny)
        else:
            contents['push_id_to_deny'] = []

        resp.media = contents
        resp.status = falcon.HTTP_OK

    def on_delete(self, req, resp, push_id):
        """
        Push.id == push_id에 해당하는 OpsPush를 Database에서 삭제.

        성공; HTTP_NO_CONTENT
        실패; HTP_NOT_FOUND

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()
            Session.delete(push)
            Session.commit()

            resp.status = falcon.HTTP_NO_CONTENT
        except NoResultFound:
            Session.rollback()
            raise falcon.HTTPNotFound()
        finally:
            Session.close()


class PushCollection(object):
    """ 다수의 OpsPush Model에 접근하는 Class """

    @validate(load_schema('push_create'))
    def on_post(self, req, resp):
        """
        1개의 OpsPush를 DB에 추가.
        json schema('push_create')를 통과해야함.

        기존 OpsPush object와 eta, expire가 겹치면 POST를 거절함.

        성공; HTTP_CREATED
        실패; HTTP_INTERNAL_SERVER_ERROR

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        # Json Schema로 Parsing 진행
        reqs = req.media

        # string으로 들어온 eta, expire를 datetime으로 변환
        reqs['eta'] = datetime.strptime(
            reqs['eta'], '%Y%m%d%H%M%S').astimezone(timezone('Asia/Seoul'))
        reqs['expire'] = datetime.strptime(
            reqs['expire'], '%Y%m%d%H%M%S').astimezone(timezone('Asia/Seoul'))

        # resq['eta']가 다른 OpsPush의 ETA ~ expire 사이면, POST 거절
        exists_query = Session.query(OpsPush).filter(
            or_(
                and_(OpsPush.eta <= reqs['eta'],
                     reqs['eta'] < OpsPush.expire),
                and_(OpsPush.eta < reqs['expire'],
                     reqs['expire'] <= OpsPush.expire),
                and_(OpsPush.eta > reqs['eta'],
                     OpsPush.expire < reqs['expire'])
            )
        ).exists()

        is_push = Session.query(exists_query).scalar()

        if is_push:
            Session.close()
            raise falcon.HTTPConflict(
                description='같은 시간에 이미 push schedule이 있습니다.')

        # OpsPush.user_extraction_id.extract_count != -1

        extract_count = None
        try:
            query_result = Session.query(OpsPushUserExtraction.extract_count).filter(
                OpsPushUserExtraction.id == reqs['ops_push_user_extraction_id'])
            query_result = query_result.one()
            extract_count = query_result[0]

        except NoResultFound as e:
            Session.close()
            raise falcon.HTTPBadRequest(
                description='해당 OpsPushUserExtraction가 Table에 존재하지 않습니다.'
            )

        if extract_count >= 0:
            # 5 min = 300 sec, 45,000개
            min_excution_time = ceil(extract_count / SEND_PER_SLICE) * MINUTE_INTERVAL * 60
            scheduled = int((reqs['expire'] - reqs['eta']).total_seconds())
            if min_excution_time > scheduled:
                Session.close()
                raise falcon.HTTPBadRequest(
                    description="ETA~Expire가 너무 짧습니다.(최소 {}분)".format(
                        min_excution_time // 60)
                )

        # push_id for except from sending list
        push_id_to_deny = reqs.pop('push_id_to_deny')

        # ORM Model 생성
        new_push_model = OpsPush(**reqs)

        # DB
        try:
            Session.add(new_push_model)
            Session.commit()

            resp.status = falcon.HTTP_CREATED
            resp.body = '/pushes/{}'.format(new_push_model.id)
        except IntegrityError:
            Session.rollback()
            Session.close()
            raise falcon.HTTPBadRequest(
                description='Foreign Key가 해당 Table에 존재하지 않습니다.'
            )

        except Exception as e:
            Session.rollback()
            Session.close()
            raise falcon.HTTPInternalServerError(
                description=e
            )

        # Update Redis "push_id_to_deny"
        if push_id_to_deny:
            redis_session.set(name="OpsPush.{}.push_id_to_deny".format(new_push_model.id),
                              value=pickle.dumps(push_id_to_deny),
                              ex=int(timedelta(days=7).total_seconds()),
                              nx=True)
        Session.close()

    def on_get(self, req, resp):
        """
        다수의 OpsPush를 포함한 json 반환

        query_string에 offset, limit은 반드시 있어야함

        state, created_at, contents_type, extraction_type으로 필터링 가능
        eta, created_at 순으로 내림차순 정렬

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        offset = req.get_param_as_int(name='offset',
                                      required=True)
        limit = req.get_param_as_int(name='limit',
                                     required=True)
        state = req.get_param(name='state')
        created_at_start = req.get_param_as_datetime(
            name="created_at_start",
            format_string="%Y%m%d%H%M%S"
        )
        created_at_end = req.get_param_as_datetime(
            name="created_at_end",
            format_string="%Y%m%d%H%M%S"
        )
        push_content_type = req.get_param(name='ops_push_content_type')
        push_user_extraction_type = req.get_param(
            name='ops_push_user_extraction_type')

        # DB
        query_result = Session.query(OpsPush,
                                     OpsPushContent.type,
                                     OpsPushUserExtraction.type)

        # Filtering
        query_result = query_result.filter(
            OpsPush.ops_push_content_id == OpsPushContent.id,
            OpsPush.ops_push_user_extraction_id == OpsPushUserExtraction.id
        )

        if state:
            """
            query_string에 state가 있으면, substring으로 필터링
            """
            query_result = query_result.filter(
                OpsPush.state.contains(state)
            )
        if created_at_start and created_at_end:
            """
            created_at 의 필터링

            postgres datetime의 resolution은 1 microsecond / 14 digits 여서
            실제론 range_start <= data in DB < range_end + 1sec로 검색함
            """
            query_result = query_result.filter(
                OpsPush.created_at >= created_at_start,
                OpsPush.created_at < created_at_end + timedelta(seconds=1)
            )
        if push_content_type:
            query_result = query_result.filter(
                OpsPushContent.type == push_content_type
            )
        if push_user_extraction_type:
            query_result = query_result.filter(
                OpsPushUserExtraction.type == push_user_extraction_type
            )

        # Ordering
        query_result = query_result.order_by(
            OpsPush.eta.desc(),
            OpsPush.created_at.desc()
        )

        # Pagination
        try:
            query_result = query_result[offset:offset + limit]
        except InvalidRequestError:
            Session.rollback()
            raise
        finally:
            Session.close()

        payload = []
        for x in query_result:
            # push 의 완료율 이다. '백분율 %'
            complete_ratio = 0

            if x[0].state not in ['created', 'tested', 'spawned']:
                # created, tested, spawned 상태라면 아직 진행된것이 아니기 때문에
                # 어차피 완료된 것이 없을 것이다.
                tasks_query = Session.query(OpsPushTask).filter(
                    OpsPushTask.ops_push_id == x[0].id
                )

                tasks_count = tasks_query.count()
                finished_tasks_count = tasks_query.filter(
                    OpsPushTask.state == 'finished'
                ).count()

                if tasks_count > 0:
                    # tasks_count 가 0보다 크다면 complete ratio 를 계산한다.
                    complete_ratio = \
                        ceil(finished_tasks_count / tasks_count * 100)

            contents = {
                "id": x[0].id,
                "created_at": x[0].created_at.strftime("%Y%m%d%H%M%S"),
                "last_modified": x[0].last_modified.strftime("%Y%m%d%H%M%S"),
                "ops_push_content_id": x[0].ops_push_content_id,
                "ops_push_user_extraction_id": x[0].ops_push_user_extraction_id,
                "task_uuid": x[0].task_uuid,
                "state": x[0].state,
                "eta": x[0].eta.strftime("%Y%m%d%H%M%S"),
                "expire": x[0].expire.strftime("%Y%m%d%H%M%S"),
                "ops_push_content_type": x[1],
                "ops_push_user_extraction_type": x[2],
                "complete_ratio": complete_ratio,
            }
            # Redis에 저장된 정보 (user_set payload에 포함시키기)
            r = redis_session.get("OpsPush.{}.userset".format(x[0].id))
            if r:
                contents["actual_user_count"] = pickle.loads(r)['count']

            # push_id for except from sending list
            push_id_to_deny = redis_session.get(
                "OpsPush.{}.push_id_to_deny".format(x[0].id))
            if push_id_to_deny:
                contents['push_id_to_deny'] = pickle.loads(push_id_to_deny)
            else:
                contents['push_id_to_deny'] = []

            payload.append(contents)
        response_body = {'payload': payload, 'count': len(payload)}

        Session.close()

        resp.media = response_body
        resp.status = falcon.HTTP_OK


class PushRelatedContents(object):
    """ 특정 푸시(push_id)가 참조하고 있는 PushContents를 요청하기 위한 Class """

    def on_get(self, req, resp, push_id):
        """
        특정 푸시(push_id)가 참조하고 있는 PushContents 반환

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()
            content = Session.query(OpsPushContent).filter(
                OpsPushContent.id == push.ops_push_content_id).one()

            resp.media = content.to_dict()
            resp.status = falcon.HTTP_OK
        except NoResultFound:
            Session.rollback()
            raise falcon.HTTPNotFound()
        finally:
            Session.close()


class PushRelatedUserExtractions(object):
    """ 특정 푸시(push_id)가 참조하고 있는 UserExtraction을 요청하기 위한 Class """

    def on_get(self, req, resp, push_id):
        """
        특정 푸시(push_id)가 참조하고 있는 UserExtraction 값을 반환

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()
            extraction = Session.query(OpsPushUserExtraction).filter(
                OpsPushUserExtraction.id == push.ops_push_user_extraction_id).one()

            resp.media = extraction.to_dict()
            resp.status = falcon.HTTP_OK
        except NoResultFound:
            Session.rollback()
            raise falcon.HTTPNotFound()
        finally:
            Session.close()
