from datetime import timedelta

import falcon

from falcon.media.validators.jsonschema import validate
from sqlalchemy.exc import DataError
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm.exc import NoResultFound

from sources.database import Session
from sources.models.ops_push import OpsPush
from sources.models.ops_push_contents import OpsPushContent
from sources.schemas import load_schema


class PushContents(object):
    """ 1개의 OpsPushContent Model에 접근하는 Class """

    def on_get(self, req, resp, push_contents_id):
        """
        하나의 OpsPushContent의 Detail 정보를 Json 형태로 반환한다.
        없는 정보 id로 요청할 경우 Non Found 를 반환한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_contents_id: OpsPushContent.id
        """

        try:
            push_contents = OpsPushContent.read_one(
                Session, id=push_contents_id)
        except NoResultFound:
            raise falcon.HTTPNotFound()
        finally:
            Session.close()

        resp.media = push_contents.to_dict()
        resp.status = falcon.HTTP_200

    @validate(load_schema('push_contents_update'))
    def on_patch(self, req, resp, push_contents_id):
        """
        수정 가능한 columns 은 (title, message, img_url, url) 이다.
        id 를 받고 수정 값은 json 형태로 받아서
        for 문을 돌려서 하나씩 수정하고 마지막에 커밋한다.
        실제로 존재하는 column 인지 체크해서 수정한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_contents_id: OpsPushContent.id
        """

        try:
            push_contents = OpsPushContent.read_one(
                Session, id=push_contents_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        data = req.media
        columns = OpsPushContent.get_columns()

        if 'id' in columns:
            columns.remove('id')

        for key in data:
            if key in columns:
                setattr(push_contents, key, data[key])
        try:
            Session.commit()
        except DataError:
            raise falcon.HTTPBadRequest(description='data error')
        except Exception:
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()

        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, push_contents_id):
        """
        id 를 받아서 삭제를 시도한다.
        Push 와 One to Many 관계여서
        하위 Push 가 있는지 확인하고 있으면
        의존성 에러를 반환한다.
        그게 아니라면 삭제를 완료한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_contents_id: OpsPushContent.id
        """
        try:
            push_contents = OpsPushContent.read_one(
                Session, id=push_contents_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        is_push = OpsPush.is_exists(
            Session, ops_push_content_id=push_contents_id)
        if is_push:
            raise falcon.HTTPLocked(
                description="상위 Push에 종속되어 있습니다."
            )

        Session.delete(push_contents)

        try:
            Session.commit()
        except Exception:
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()

        resp.status = falcon.HTTP_NO_CONTENT


class PushContentsCollection(object):
    """
    다수의 OpsPushContent Model에 접근하는 Class
    """

    def on_get(self, req, resp):
        """
        Push Contents의 Collection[offset:offset+limit]을 반환한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object

        req; query_string 'offset' (required),
             query_string 'limit' (required),
             query_string 'tag',
             query_string 'type',
             query_string 'last_modified_range_start',
             query_string 'last_modified_range_end',
             query_string 'reverse'
             를 입력받아서 DB에 해당 field로 검색함

        resp; json {
                payload: [list] (ops_push_content.id desc 로 정렬한 list),
                count: integer (payload item 개수)
                }

        """
        # Query string parsing
        offset = req.get_param_as_int(name="offset",
                                      required=True)
        limit = req.get_param_as_int(name="limit",
                                     required=True)
        tag = req.get_param(name="tag")
        type = req.get_param(name="type")
        last_modified_range_start = req.get_param_as_datetime(
            name="last_modified_range_start",
            format_string="%Y%m%d%H%M%S"
        )
        last_modified_range_end = req.get_param_as_datetime(
            name="last_modified_range_end",
            format_string="%Y%m%d%H%M%S"
        )
        reverse_order = req.get_param_as_bool(name="reverse")
        is_ad = req.get_param_as_bool(name="is_ad")

        query_result = Session.query(OpsPushContent)
        if tag:
            """
            tag에 대해 substring 검색
            """
            query_result = query_result.filter(
                OpsPushContent.tag.contains(tag))
        if type:
            """
            type 에 대해 substring 검색
            """
            query_result = query_result.filter(
                OpsPushContent.type == type)

        if last_modified_range_start and last_modified_range_end:
            """
            last_modified에 대해서 datetime_range가 들어오면,
            (요구사항) range_start <= data in DB <= range_end 검색
            postgres datetime의 resolution은 1 microsecond / 14 digits 여서
            실제론 range_start <= data in DB < range_end + 1sec로 검색함
            """
            query_result = query_result.filter(
                OpsPushContent.last_modified >= last_modified_range_start,
                OpsPushContent.last_modified < last_modified_range_end +
                timedelta(seconds=1))
        if is_ad is not None:
            query_result = query_result.filter(
                OpsPushContent.is_ad == is_ad
            )

        if reverse_order:
            """ if reverse_order (last_modified inc order)"""
            query_result = query_result.order_by(
                OpsPushContent.last_modified)
        else:
            """ Default (last_modified desc order)"""
            query_result = query_result.order_by(
                OpsPushContent.last_modified.desc())

        try:
            query_result = query_result[offset:offset + limit]
        except InvalidRequestError:
            Session.rollback()
            raise
        finally:
            Session.close()

        """
        response body 생성
        """
        payload = []
        for x in query_result:
            payload.append(x.to_dict())
        response_body = {"payload": payload, "count": len(payload)}

        resp.media = response_body

    @validate(load_schema('push_contents_create'))
    def on_post(self, req, resp):
        """
        PushContentsCollection으로 들어온 POST 요청 처리
        Request의 Body(json)를 파싱해 Model을 만들고, DB에 저장함

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object

        """
        reqs = req.media
        new_push_contents = OpsPushContent(**reqs)

        # DB Connection
        Session.add(new_push_contents)
        try:
            Session.commit()

            resp.status = falcon.HTTP_CREATED
            resp.body = '/pushcontents/{}'.format(new_push_contents.id)
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()
