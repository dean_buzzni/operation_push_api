import falcon

from celery import uuid
from datetime import datetime
from pytz import timezone

from sqlalchemy.orm.exc import NoResultFound

from sources.consumer import app
from sources.consumer import resume_push
from sources.consumer import spawn
from sources.consumer import test_push_send
from sources.database import Session
from sources.models.ops_push import OpsPush
from sources.models.ops_push_task import OpsPushTask


class PushSpawn(object):
    """ Celery spawn-worker에 접근하는 Class """

    def on_post(self, req, resp, push_id):
        """
        push_id 를 받아서 해당 push 를 Spawn Queue 에 enqueue 한다

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            raise falcon.HTTPNotFound()

        if push.state != 'tested':
            raise falcon.HTTPBadRequest(
                description='push.state must be "tested"'
            )

        # push.eta가 지난 이후에 spawn인 경우 실패처리
        if push.eta < datetime.now(tz=timezone('Asia/Seoul')):
            push.state = 'failed'
            Session.commit()
            Session.close()
            raise falcon.HTTPBadRequest(description='now > push.eta')

        reserved_uuid = uuid()
        push.task_uuid = reserved_uuid
        push.state = 'spawned'
        try:
            Session.commit()
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(
                description='OpsPush update failed.'
            )
        resp.status = falcon.HTTP_ACCEPTED
        resp.body = reserved_uuid

        r = spawn.apply_async(queue='spawn',
                              task_id=reserved_uuid,
                              kwargs={'push_id': push_id},
                              eta=push.eta,
                              expires=push.expire)
        Session.close()

        if r.id is None:
            raise falcon.HTTPInternalServerError(
                description='Spawning celery task failed.'
            )


class PushStop(object):
    """
    특정 Push와 관련된 Task들을 Failed 처리함.

    비동기적 요소가 없어서 Celery Work 를 사용하지 않음
    """

    def on_post(self, req, resp, push_id):
        """
        push_id 를 정지 한다. 다시 실행할 수 없으며 해당 Push를 fail처리함.

        선택된 push 아래 하위 task 중에 'ready' 인 상태의 task 들만 revoke 시킨다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        uuid_list = []

        if push.state in ['ready', 'sending']:
            # ready, sending 은 task 가 만들어진 상태이기 때문에 부가적인
            # 작업이 필요하다.
            push_task_list = Session.query(OpsPushTask).filter(
                OpsPushTask.ops_push_id == push_id) \
                .filter(OpsPushTask.state == 'ready').all()

            if not push_task_list:
                raise falcon.HTTPInternalServerError(
                    description="push 하위 task 중 'ready' 인 상태가 없습니다."
                )

            for push_task in push_task_list:
                uuid_list.append(push_task.task_uuid)
                push_task.state = 'failed'
        elif push.state not in ['created', 'tested', 'spawned']:
            # ready, sending, created, tested, spawend 상태가 아닐 경우
            # stop 할 수 없다.
            Session.close()
            raise falcon.HTTPBadRequest(
                description='push.state must be "created, tested, spawned, '
                            'ready, sending"'
            )

        push.state = 'failed'

        try:
            Session.commit()
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(
                description="OpsPush update failed"
            )
        else:
            if uuid_list:
                app.control.revoke(uuid_list)
        finally:
            Session.close()

        resp.status = falcon.HTTP_OK


class PushPause(object):
    """
    특정 Push와 관련된 Task들을 Paused 처리함.

    비동기적 요소가 없어서 Celery Work 를 사용하지 않음
    """

    def on_post(self, req, resp, push_id):
        """
        push_id 를 일시정지 한다. stop 과는 다르게 다시 시작할 수 있다.

        선택된 push 아래 하위 task 중에 'ready' 인 상태의 task 들만 revoke 시킨다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        if push.state != 'sending':
            # sending 상태의 push 만 pause 할 수 있다.
            Session.close()
            raise falcon.HTTPBadRequest(
                description='push.state must be "sending"'
            )

        push_task_list = Session.query(OpsPushTask).filter(
            OpsPushTask.ops_push_id == push_id) \
            .filter(OpsPushTask.state == 'ready').all()

        if not push_task_list:
            raise falcon.HTTPInternalServerError(
                description="push 하위 task 중 'ready' 인 상태가 없습니다."
            )

        uuid_list = []
        for push_task in push_task_list:
            uuid_list.append(push_task.task_uuid)
            push_task.state = 'paused'

        push.state = 'paused'

        try:
            Session.commit()
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(
                description="OpsPush update failed"
            )
        else:
            app.control.revoke(uuid_list)
        finally:
            Session.close()

        resp.status = falcon.HTTP_OK


class PushResume(object):
    """ Celery worker, 특정 Push와 관련된 Task들을 Resume 처리함. """

    def on_post(self, req, resp, push_id):
        """
        push_id 를 받아서 해당 push 를 다시 시작 한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        # Task 에 넘겨서 처리하기
        if push.state != 'paused':
            # paused 상태만 resume 가능하다
            Session.close()
            raise falcon.HTTPBadRequest(
                description='push.state must be "paused"'
            )

        if push.expire < datetime.now(tz=timezone('Asia/Seoul')):
            push.state = 'failed'

            try:
                Session.commit()
            except Exception:
                raise falcon.HTTPInternalServerError()
            else:
                raise falcon.HTTPBadRequest(
                    description="Push expire 가 지났습니다."
                )
            finally:
                Session.close()
        else:
            Session.close()

            r = resume_push.apply_async(queue='resume',
                                        kwargs={'push_id': push_id})
            resp.status = falcon.HTTP_ACCEPTED


class PushTest(object):
    """ Celery worker, 특정 Push를 Test 발송한다. """

    def on_post(self, req, resp, push_id):
        """
        push_id 에 해당하는 푸쉬를 시스템에 등록된 테스트 유저에게 보낸다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id

        """
        try:
            push = OpsPush.read_one(Session, id=push_id)
        except NoResultFound:
            raise falcon.HTTPNotFound()
        finally:
            Session.close()

        if push.state not in ['created', 'tested', 'spawned']:
            raise falcon.HTTPBadRequest(
                description="push({id}).state = {state} (must be 'created', 'tested' or 'spawned')".format(
                    id=push_id, state=push.state)
            )

        test_push_send.apply_async(queue='test',
                                   kwargs={'push_id': push_id})
        resp.status = falcon.HTTP_ACCEPTED
