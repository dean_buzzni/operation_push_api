import falcon
import pickle

from datetime import timedelta
from secrets import token_urlsafe
from sources.database import Session
from sources.database import redis_session


class DownloadCSV(object):
    """ Docstring for DownloadCSV """

    def on_get(self, req, resp, push_id):
        """
        OpsPush.id에 해당하는 user_set을 csv file로 반환

        r = redis_session.get("OpsPush.{}.userset".format(OpsPush.id))
        r['android'], r['ios']의 user_id만 CSV 형식으로 반환

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_id: Push object id
        """
        r = redis_session.get("OpsPush.{}.userset".format(push_id))
        be_csv = "num,xid\n"
        line_number = 1
        if r:
            r = pickle.loads(r)
            for tupled_item in r['android']:
                be_csv = be_csv + "{},{}\n".format(line_number, tupled_item[0])
                line_number = line_number + 1
            for tupled_item in r['ios']:
                be_csv = be_csv + "{},{}\n".format(line_number, tupled_item[0])
                line_number = line_number + 1
            resp.body = be_csv
            resp.downloadable_as = "operation_push_{}.csv".format(push_id)
            resp.content_type = 'text/csv'
        else:
            raise falcon.HTTPNotFound()


class UploadCSV(object):
    """ Docstring for UploadCSV """

    def on_post(self, req, resp):
        """
        요청으로 들어온 csv file을 redis_cache에 저장하고 key를 반환

        이후 comsumer.spawn()에서 user_extraction.type == 'csv'일 때,
        redis_session.get(csv_stored_key)로 user_id를 받고
        userid_to_token_extraction(set)에 넣어 Push를 진행한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        csv_token = token_urlsafe(16)

        file_stream = req.bounded_stream.read().decode('utf-8')
        contents = file_stream.split('\r')[4].strip()

        # Check csv file validity
        valid_csv_headline = "num,xid"
        if contents.split('\n')[0] != valid_csv_headline:
            raise falcon.HTTPBadRequest()
        else:
            redis_session.set(
                name='csv_{}'.format(csv_token),
                value=pickle.dumps(contents),
                ex=int(timedelta(days=30).total_seconds()),
                nx=False
            )
            resp.media = {"csv_key": 'csv_{}'.format(csv_token)}
            resp.status = falcon.HTTP_200
