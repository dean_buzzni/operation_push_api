import falcon

from datetime import datetime
from datetime import timedelta
from falcon.media.validators.jsonschema import validate
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm.exc import NoResultFound

from sources.consumer import estimate_extraction
from sources.database import Session
from sources.database import redis_session
from sources.models.ops_push import OpsPush
from sources.models.ops_push_user_extraction import OpsPushUserExtraction
from sources.schemas import load_schema


class UserExtraction(object):
    """id에 해당하는 한개의 User Extraction 처리"""

    def on_get(self, req, resp, push_user_extraction_id):
        """User Extraction 을 조회한다.

        id에 해당되는 User Extration 을 조회해서 Json 형태로 반환

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_user_extraction_id: OpsPushUserExtraction.id
        """
        try:
            query_result = OpsPushUserExtraction.\
                read_one(Session, id=push_user_extraction_id)
        except NoResultFound:
            raise falcon.HTTPNotFound()
        finally:
            Session.close()

        resp.media = query_result.to_dict()
        resp.status = falcon.HTTP_OK

    def on_delete(self, req, resp, push_user_extraction_id):
        """User Extraction 을 삭제한다.

        id에 해당되는 값을 삭제한다. 성공하면 삭제해서 컨텐츠가 없다는 의미로
        NO_CONTENT 즉 '204'를 반환한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_user_extraction_id: OpsPushUserExtraction.id
        """
        try:
            query_result = OpsPushUserExtraction.\
                read_one(Session, id=push_user_extraction_id)
        except NoResultFound:
            Session.close()
            raise falcon.HTTPNotFound()

        is_push = OpsPush.is_exists(
            Session, ops_push_user_extraction_id=push_user_extraction_id)
        if is_push:
            raise falcon.HTTPLocked(
                description="상위 Push에 종속되어 있습니다."
            )

        Session.delete(query_result)

        try:
            Session.commit()
        except Exception:
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()

        resp.status = falcon.HTTP_NO_CONTENT


class UserExtractionCollection(object):
    """docstring for UserExtractionCollection."""

    def on_get(self, req, resp):
        """
        다수의 OpsPushUserExtraction을 Json 형태로 반환.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        """
        offset = req.get_param_as_int(name="offset",
                                      required=True)
        limit = req.get_param_as_int(name="limit",
                                     required=True)

        os = req.get_param(name="os")
        type = req.get_param(name="type")
        value = req.get_param(name="value")

        query_result = Session.query(OpsPushUserExtraction)

        # Filtering
        if os:
            """
            OS filtering ['both', 'andriod', 'ios']
            """
            query_result = query_result.filter(
                OpsPushUserExtraction.os.contains(os))
        if type:
            """
            type 에 대해 substring 검색
            """
            query_result = query_result.filter(
                OpsPushUserExtraction.type.contains(type))
        if value:
            """
            value에 대해 substring 검색
            """
            query_result = query_result.filter(
                OpsPushUserExtraction.value.contains(value)
            )

        # Sorting
        query_result = query_result.order_by(
            OpsPushUserExtraction.id.desc())

        # Pagination
        try:
            query_result = query_result[offset:offset + limit]
        except InvalidRequestError:
            Session.rollback()
            raise
        finally:
            Session.close()

        payload = []
        for x in query_result:
            payload.append(x.to_dict())
        response_body = {"payload": payload, "count": len(payload)}

        resp.media = response_body

    @validate(load_schema('push_user_extraction_create'))
    def on_post(self, req, resp):
        """
        json 형태로 들어온 데이터를 OpsPushUserExtraction Model로 바꿔
        Database에 추가함

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object

        req; json
            {
                "os":"string" required,
                "date_offset":"string" '%Y%m%d%H%M%S' format,
                "day_interval":"integer" less than 0 and required,
                "extract_count":"integer" -1 or grater than zero,
                "type":"string" one of
                    ["ordinary", "moaweek", "search_personalize",
                     "dormant", "test"],
                "value":"string" (Nullable)
            }
        """
        reqs = req.media
        # integer로 들어온 day_interval을 저장을 위해 datetime.timedelta type 로 바꿈
        reqs['day_interval'] = timedelta(days=reqs['day_interval'])

        # Buzzni Datetime String Format('%Y%m%d%H%M%S')
        # >> python datetime object로 변환한다.
        if 'date_offset' in reqs:
            reqs['date_offset'] = datetime.strptime(reqs['date_offset'],
                                                    '%Y%m%d%H%M%S')

        # OpsPushUserExtraction ORM Model 생성
        new_push_user_extraction = OpsPushUserExtraction(**reqs)

        # DB Connection
        Session.add(new_push_user_extraction)

        try:
            Session.flush()
            resp.status = falcon.HTTP_CREATED
            resp.body = '/userextractions/{}'.format(
                new_push_user_extraction.id)
            Session.commit()
        except Exception:
            Session.rollback()
            raise falcon.HTTPInternalServerError(description='db commit error')
        finally:
            Session.close()


class UserExtractionEstimate(object):
    """id에 해당하는 한개의 User Extraction 에 대한 견적 처리"""

    def on_get(self, req, resp, push_user_extraction_id):
        """
        넘어온 id에 대한 user_extraction Count 를 보여주거나 요청한다.

        1. push_user_extraction_id를 받아서 Cache 에 해당 값이 있는지 확인한다.
           해당 값이 캐시에 있으면 바로 Cache 값과 HTTP_OK를 반환한다.
        2. 없다면 worker에게 estimate 작업을 요청하고 HTTP_ACCEPTED 를 반환한다.
        3. DB에서 조회되지 않거나 다수인 extraction 이라면 HTTP_NOT_FOUND 반환한다.

        :param falcon.Request req: falcon의 Request object
        :param falcon.Response resp: falcon의 Request object
        :param int push_user_extraction_id: OpsPushUserExtraction.id
        """
        cached_count = redis_session.get(
            name="OpsPushUserExtraction.{}.estimate".format(
                push_user_extraction_id))

        if cached_count:
            # 해당 extraction_id 에 대한 estimate의 cache가 있다면
            # Redis를 참조해 반환한다.
            resp.status = falcon.HTTP_OK
            resp.media = {"count": int(cached_count)}

        else:
            # Estimate Cache가 없다면, Estimate lock을 확인하고 worker에게 요청한다.
            lock_key = "OpsPushUserExtraction.{}.estimate.lock".format(
                push_user_extraction_id)

            if redis_session.exists(name=lock_key):
                # cached_count가 없지만, 이미 worker에서 계산중일 때
                resp.status = falcon.HTTP_226

            else:
                # worker에게 estimated_count 계산을 요청한다.
                is_valid_extraction = OpsPushUserExtraction.\
                    is_exists(Session, id=push_user_extraction_id)
                Session.close()

                if is_valid_extraction:
                    estimate_extraction.apply_async(
                        queue='test',
                        kwargs={
                            'push_user_extraction_id': push_user_extraction_id
                        })
                    resp.status = falcon.HTTP_ACCEPTED
                else:
                    raise falcon.HTTPNotFound()
