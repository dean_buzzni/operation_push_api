import requests

from datetime import datetime

FCM_URL = 'https://fcm.googleapis.com/fcm/send'
ANDROID_API_KEY = "AIzaSyDsbNg7PppEX1AeH9hJt9gt9K3iG1MCKRI"
HEADERS = {'Content-Type': 'application/json',
           'Authorization': "key=" + ANDROID_API_KEY}

FCM_MAX_RECIPIENTS = 1000

def push_android_user(device_id_list, title, body, url, img_url='', extra=None,
                      sound="found_item", priority="high", is_popup=True, push_id=0):
    """
    device_id_list에 명시된 모든 device에 푸시를 전송하기 위해 FCM으로 Post 요청

    :param list device_id_list: device id list
    :param str title: Push 타이틀
    :param str body: Push 본문
    :param str url: 푸시를 클릭하면 이동할 URL
    :param str img_url: 팝업에 나올 이미지
    :param dict extra: 푸시에 필요한 추가 데이터
    :param str sound: default값 사용
    :param str priority: default값 사용
    :param bool is_popup: 팝업을 띄울지 유무
    :param int push_id: ops_push table의 primary key
    """
    if sound == 'off':
        sound = ''

    # Kibana
    KIBANA_QUERY_STRING = "&from=ops_push&pushid={}".format(push_id)

    url = url + KIBANA_QUERY_STRING

    value = {"title": title, "msg": body, "url": url, "sound": sound}

    data = {}
    each = {}
    if is_popup:
        data = {'command': 'popup'}
        each = {
            'img': img_url,
            'link': url,
            'screen_sec': 0,
            'version': 1.0,
            'cate': '',
            'name': '',
            'user_id': '',
            'genre': '',
            'supplier': 'buzzni',
            'entity_id': 0,
            'alert_msg': '',
            'point': 0,
            'valid_days': 0,
        }

    # 'result' 에 대한 값이 없다면 푸쉬가 가지 않는다.
    if extra is None:
        extra = {'result': ([each], 1)}
    elif 'result' not in extra:
        extra.update({"result": ([each], 1)})

    if img_url:
        extra.update({'banner': img_url})

    value.update({"args": extra})

    start = datetime.now()

    for i in range(0, len(device_id_list), FCM_MAX_RECIPIENTS):
        chunk = device_id_list[i:i + FCM_MAX_RECIPIENTS]

        data.update({
            'registration_ids': chunk,
            'priority': priority,
            'data': value
        })

        try:
            requests.post(url=FCM_URL, json=data, headers=HEADERS)
        except Exception as e:
            print(e)
            continue

    end = datetime.now()
    print("fcm request: {}".format(end - start))
