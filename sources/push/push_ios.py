from apns2.client import APNsClient
from apns2.client import Notification
from apns2.payload import Payload

from datetime import datetime
from os import path


KEYS_ROOT_PATH = path.dirname(__file__) + "/keys/"
IOS_HOMESHOPPING_CERT_FILE = KEYS_ROOT_PATH + 'tvshop_cert.pem'
IOS_TOPIC = 'buzzni.homeshoppingmoa.webapp'


def push_ios_user(device_id_list, title, body, url, img_url='', extra=None,
                  sound='found_item', badge=0, mutable_content=True, push_id=0):
    """
    device_id_list에 명시된 모든 device에 푸시를 전송하기 위해
    apns2 라이브러리를 사용함

    :param list device_id_list: device id list
    :param str title: Push 타이틀
    :param str body: Push 본문
    :param str url: 푸시를 클릭하면 이동할 URL
    :param str img_url: 팝업에 나올 이미지
    :param dict extra: 푸시에 필요한 추가 데이터
    :param str sound: default값 사용
    :param int badge: 앱에 표시되는 New 숫자
    :param bool mutable_content: default값 사용
    :param int push_id: ops_push table의 primary key
    """

    alert = {'title': title, 'body': body}

    if extra is None:
        extra = {}

    if img_url:
        extra.update({'popup': img_url})

    #  앱에서 주어진 url로 이동하는 방식이 android와 달라서 Domain url을 추가해야함
    DOMAIN = "http://o.buzzni.com"

    # Kibana
    KIBANA_QUERY_STRING = "&from=ops_push&pushid={}".format(push_id)

    url = DOMAIN + url + KIBANA_QUERY_STRING
    extra.update({"tag": url})
    extra.update({"c": 0})

    if sound == 'off':
        sound = ''
    elif sound:
        sound = '{}.wav'.format(sound)

    payload = Payload(
        alert=alert, sound=sound, badge=badge,
        custom=extra, mutable_content=mutable_content)

    apns_client = APNsClient(IOS_HOMESHOPPING_CERT_FILE, use_sandbox=False)

    notifications = []

    for idx, device_id in enumerate(device_id_list):
        notifications.append(Notification(device_id, payload))

    start = datetime.now()

    try:
        apns_client.send_notification_batch(
            notifications, IOS_TOPIC, expiration=3600)
    except Exception as e:
        print(e)
    finally:
        end = datetime.now()
        print("apns request: {}".format(end - start))
