import falcon

from falcon_cors import CORS

from sqlalchemy.schema import MetaData

from sources.database import Base
from sources.database import engine
from sources.resources.push_contents import PushContents
from sources.resources.push_contents import PushContentsCollection
from sources.resources.user_extraction import UserExtraction
from sources.resources.user_extraction import UserExtractionCollection
from sources.resources.user_extraction import UserExtractionEstimate

from sources.resources.push import Push
from sources.resources.push import PushCollection
from sources.resources.push import PushRelatedContents
from sources.resources.push import PushRelatedUserExtractions
from sources.resources.push_controller import PushPause
from sources.resources.push_controller import PushResume
from sources.resources.push_controller import PushSpawn
from sources.resources.push_controller import PushStop
from sources.resources.push_controller import PushTest
from sources.resources.push_test_receiver import PushTestReceiver
from sources.resources.push_test_receiver import PushTestReceiverCollection
from sources.resources.user_id_csv import DownloadCSV
from sources.resources.user_id_csv import UploadCSV

print("@Producer")
Base.metadata = MetaData(bind=engine)

cors = CORS(allow_all_origins=True,
            allow_all_headers=True,
            allow_all_methods=True
            )
app = falcon.API(middleware=[cors.middleware])

app.add_route('/pushcontents', PushContentsCollection())
app.add_route('/pushcontents/{push_contents_id:int}', PushContents())

app.add_route('/userextractions', UserExtractionCollection())
app.add_route(
    '/userextractions/{push_user_extraction_id:int}', UserExtraction())
app.add_route(
    '/userextractions/{push_user_extraction_id:int}/estimate',
    UserExtractionEstimate())

app.add_route('/pushes', PushCollection())
app.add_route('/pushes/{push_id:int}', Push())
app.add_route('/pushes/{push_id:int}/spawn', PushSpawn())
app.add_route('/pushes/{push_id:int}/stop', PushStop())
app.add_route('/pushes/{push_id:int}/pause', PushPause())
app.add_route('/pushes/{push_id:int}/resume', PushResume())
app.add_route('/pushes/{push_id:int}/test', PushTest())
app.add_route('/pushes/{push_id:int}/csv', DownloadCSV())
# pushes related API
app.add_route('/pushes/{push_id:int}/pushcontents', PushRelatedContents())
app.add_route('/pushes/{push_id:int}/userextractions',
              PushRelatedUserExtractions())

app.add_route('/testreceivers', PushTestReceiverCollection())
app.add_route('/testreceivers/{test_reciver_id:int}', PushTestReceiver())

app.add_route('/csv', UploadCSV())
