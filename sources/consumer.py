import gc
import pickle
import random

from celery import Celery
from celery import Task
from celery import uuid
from celery.signals import task_failure
from celery.signals import task_revoked
from celery.signals import task_success
from datetime import datetime
from datetime import timedelta
from math import ceil
from pytz import timezone

from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound

from sources.database import Session
from sources.database import redis_session
from sources.extraction import csv_user_extraction
from sources.extraction import event_user_extraction
from sources.extraction import keywords_user_extraction
from sources.extraction import ordinary_user_extraction
from sources.extraction import test_user_extraction
from sources.extraction import userid_to_token_extraction

from sources.models.ops_push import OpsPush
from sources.models.ops_push_contents import OpsPushContent
from sources.models.ops_push_task import OpsPushTask
from sources.models.ops_push_test_receiver import OpsPushTestReceiver
from sources.models.ops_push_user_extraction import OpsPushUserExtraction
from sources.push.push_android import push_android_user
from sources.push.push_ios import push_ios_user
from sources.conf import SEND_PER_SLICE
from sources.conf import MINUTE_INTERVAL

app = Celery('worker', broker='amqp://rabbitmq')

task_route = {
    'worker.spawn': {'queue': 'spawn'},
    'worker.send_android': {'queue': 'ready'},
    'worker.send_ios': {'queue': 'ready'},
    'worker.teardown': {'queue': 'finish'},
    'worker.test_push_send': {'queue': 'test'},
    'worker.estimate_extraction': {'queue': 'test'},
    'worker.resume_push': {'queue': 'resume'},
}
app.conf.task_route = task_route
app.conf.worker_prefetch_multiplier = 1
app.conf.enable_utc = False
app.conf.timezone = 'Asia/Seoul'


class EstimateExtractionBase(Task):
    """
    estimate_extraction Worker의 Base Class

    해당 Celery worker의 결과, 액션에 따른 처리를 한다.
    """

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        특정 Celery worker에서 exception이 발생했을 때

        에러가 났을때 redis의 lock을 지운다.
        전체 워커에 대한 task_failure 함수가 있다면
        해당 메서드를 처리하고 단계적으로 실행된다.

        :param type exc: desc
        :param type task_id: desc
        :param type args: desc
        :param type kwargs: desc
        :param type einfo: desc
        """
        push_user_extraction_id = kwargs.get('push_user_extraction_id', 0)
        lock_key = "OpsPushUserExtraction.{}.estimate.lock".format(
            push_user_extraction_id)
        redis_session.delete(lock_key)


@task_revoked.connect
def revoked_handler(*args, **kwargs):
    """
    모든 task에 revoke 가 들어온다.

    revoke 를 요청하는 시점이 아니라 실제 rovoke 되는 시점에 발생
    만료시간 때문에 revoke 되었다면 해당 task 는 failed 처리 해준다.

    :param type args: desc
    :param type kwargs: desc
    """
    if kwargs.get('expired'):
        task_uuid = kwargs.get('request').id

        try:
            query_result = Session.query(OpsPushTask).filter(
                OpsPushTask.task_uuid == task_uuid).one()
            query_result.state = 'failed'
            Session.commit()
        except Exception:
            Session.rollback()
        finally:
            Session.close()


@task_success.connect
def success_handler(*args, **kwargs):
    """
    성공한 모든 task에 대한 작업

    쓰지 않는 메모리를 release 해준다.
    """
    gc.collect()


@task_failure.connect
def failure_handler(task_id, exception, *args, **kwargs):
    """실패한 모든 task에 대한 작업

    1. 열려있는 Session 을 공통적으로 닫아준다.
    2. 쓰지 않는 메모리를 release 해준다.
    3. exception 내용을 print 한다.
    """
    Session.rollback()
    Session.close()
    gc.collect()


@app.task(bind=True)
def spawn(self, push_id):
    """
    Test 끝난 Push_id 대상으로 /push/{push_id}/spawn 호출로 생성됨

    spawn queue에 들어온 task들은 ETA까지 대기상태.
    ETA가 지나면 worker가 깨어나 push 받을 사용자를 추출하고
    발송 속도를 조절하기 위해 다수의 sub task를 생성해 push를 발송합니다.


    :param int push_id: ops_push table의 primary key
    """
    print("spawn()... {}".format(push_id))

    push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()

    if push.state not in ['spawned']:
        print("spawn() Validation Error. push.state = '{}' (Must be 'spawned')".format(
            push.state))
        Session.close()
        return False

    # 1. 유저 추출 준비
    try:
        user_extraction = Session.query(
            OpsPushUserExtraction.id,
            OpsPushUserExtraction.extract_count,
            OpsPushUserExtraction.os,
            OpsPushUserExtraction.type
        ).filter(
            OpsPushUserExtraction.id == push.ops_push_user_extraction_id
        ).one()

    except NoResultFound:
        # push와 매칭되는 OpsPushUserExtraction이 없음
        push.state = 'failed'
        Session.commit()
        raise

    user_set = get_user_set_by(user_extraction.id)
    android_users = user_set['android']
    ios_users = user_set['ios']

    # OpsPushUserExtraction.os filter
    if user_extraction.os == 'android':
        ios_users = set()
    elif user_extraction.os == 'ios':
        android_users = set()

    # 유저가 선택한 푸시 대상자 제거 (OpsPush.id에 해당하는 푸시를 받은사람 제거)
    redis_output = redis_session.get(
        "OpsPush.{}.push_id_to_deny".format(push_id))
    if redis_output:
        push_ids_to_deny = pickle.loads(redis_output)
        deny_user_set = set()  # {'user_id_1', ..., 'user_id_n'}
        for deny_id in push_ids_to_deny:
            r = redis_session.get("OpsPush.{}.userset".format(deny_id))
            if r:
                r = pickle.loads(r)
                for x in r['android']:
                    deny_user_set.add(x[0])
                for x in r['ios']:
                    deny_user_set.add(x[0])

        # android_users 에서 deny_user_set에 포함된 'user_id' 제거
        android_users = set(
            filter(lambda x: x[0] not in deny_user_set, android_users))

        # ios_users 에서 deny_user_set에 포함된 'user_id' 제거
        ios_users = set(filter(lambda x: x[0] not in deny_user_set, ios_users))

    """
    OpsPushUserExtraction.extract_count filter
    extraction_count가 정해져있으면, 전체 user_set에서 랜덤으로 count 갯수만큼 선택한다.
    """
    if user_extraction.extract_count > -1:
        if len(android_users) + len(ios_users) > user_extraction.extract_count:
            whole_set = set()
            for x in android_users:
                whole_set.add(('android', x[0], x[1]))
            for x in ios_users:
                whole_set.add(('ios', x[0], x[1]))
            whole_set = random.sample(whole_set, user_extraction.extract_count)
            choosen_android = set()
            choosen_ios = set()
            for x in whole_set:
                if x[0] == 'android':
                    choosen_android.add((x[1], x[2]))
                else:
                    choosen_ios.add((x[1], x[2]))
            android_users = choosen_android
            ios_users = choosen_ios

    # Update Redis fixed set (key = OpsPush.push_id.userset)
    pickled = pickle.dumps(
        {'android': android_users,
         'ios': ios_users,
         'count': len(android_users) + len(ios_users)}
    )
    redis_session.set(name="OpsPush.{}.userset".format(push_id),
                      value=pickled,
                      ex=int(timedelta(days=7).total_seconds()),
                      nx=True)

    # 필터링이 끝나고 use_set을 list로 변환
    android_users = list(android_users)
    ios_users = list(ios_users)
    total_user_size = len(android_users) + len(ios_users)

    # 만약 보낼 Push가 없으면 push.state 바꾸고 종료
    if total_user_size == 0:
        push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()
        push.state = 'finished'
        Session.commit()
        return "empty user_set"

    # Get # of split task
    send_per_slice = SEND_PER_SLICE

    # Unlimited sending (dormant user)
    if user_extraction.type == 'dormant':
        send_per_slice = 10000000  # MINUTE_INTERVAL (분)당 1천만개 보낼수 있게 함 (push limit 해제)

    send_android_per_slice = ceil(
        send_per_slice * len(android_users) / total_user_size)
    send_ios_per_slice = ceil(
        send_per_slice * len(ios_users) / total_user_size)

    if send_android_per_slice + send_ios_per_slice > send_per_slice:
        if send_android_per_slice > send_ios_per_slice:
            send_android_per_slice = send_android_per_slice - 1
        else:
            send_ios_per_slice = send_ios_per_slice - 1

    # MINUTE_INTERVAL 분 단위로 몇개의 task가 만들어져야 하나?
    try:
        android_task_slice = ceil(len(android_users) / send_android_per_slice)
    except ZeroDivisionError:
        android_task_slice = 0
    try:
        ios_task_slice = ceil(len(ios_users) / send_ios_per_slice)
    except ZeroDivisionError:
        ios_task_slice = 0
    estimated_task_slice = android_task_slice + ios_task_slice
    # Celery uuid Pool
    celery_task_pool = []
    for x in range(estimated_task_slice):
        celery_task_pool.append(uuid())

    # Pre-create OpsPushTask
    for preserved_uuid in celery_task_pool:
        ops_push_task = OpsPushTask(ops_push_id=push_id,
                                    task_uuid=preserved_uuid,
                                    state='ready')
        Session.add(ops_push_task)

    try:
        Session.commit()
    except Exception:
        Session.rollback()

        # OpsPushTask DB write 실패, Push.state = 'failed'
        push.state = 'failed'
        Session.commit()
        raise

    # 5. Update OpsPush.test
    push.state = 'ready'
    Session.commit()

    # 2. ETA ~ Expire 까지 MINUTE_INTERVAL 분 단위로 slicing 해서 user_set 나누기

    # Android_send task
    accumulate_eta = push.eta
    for x in range(android_task_slice):
        user_to_send = android_users[x *
                                     send_android_per_slice:(x + 1) * send_android_per_slice]
        preserved_uuid = celery_task_pool.pop()
        # 3.Redis store ("uuid": pickled ), expires after 7 days
        pickled = pickle.dumps({'os': 'android', 'user_set': user_to_send})
        a_week_after = int(timedelta(days=7).total_seconds())
        redis_session.set(name=preserved_uuid,
                          value=pickled,
                          ex=a_week_after,
                          nx=True)

        estimated_eta = accumulate_eta
        estimated_expire = estimated_eta + timedelta(minutes=MINUTE_INTERVAL)

        if estimated_expire >= push.expire:
            estimated_expire = push.expire

        # android_device_list = [x[1] for x in user_to_send]

        android_ready_task = send_android.apply_async(
            queue='ready',
            task_id=preserved_uuid,
            kwargs={
                'push_id': push_id,
                'redis_token_key': preserved_uuid},
            eta=estimated_eta,
            expires=estimated_expire
        )
        accumulate_eta = accumulate_eta + timedelta(minutes=MINUTE_INTERVAL)

    # Ios_send task
    accumulate_eta = push.eta
    for x in range(ios_task_slice):
        user_to_send = ios_users[x *
                                 send_ios_per_slice:(x + 1) * send_ios_per_slice]
        preserved_uuid = celery_task_pool.pop()

        # 3.Redis store ("uuid": pickled ), expires after 7 days
        pickled = pickle.dumps({'os': 'ios', 'user_set': user_to_send})
        a_week_after = int(timedelta(days=7).total_seconds())
        redis_session.set(name=preserved_uuid,
                          value=pickled,
                          ex=a_week_after,
                          nx=True)
        estimated_eta = accumulate_eta
        estimated_expire = estimated_eta + timedelta(minutes=MINUTE_INTERVAL)

        if estimated_expire >= push.expire:
            estimated_expire = push.expire

        # ios_device_list = [x[1] for x in user_to_send]

        ios_ready_task = send_ios.apply_async(
            queue='ready',
            task_id=preserved_uuid,
            kwargs={
                'push_id': push_id,
                'redis_token_key': preserved_uuid},
            eta=estimated_eta,
            expires=estimated_expire
        )
        accumulate_eta = accumulate_eta + timedelta(minutes=MINUTE_INTERVAL)
    Session.close()
    return "push.{} spawned.".format(push_id)


@app.task(bind=True)
def send_ios(self, push_id, redis_token_key):
    """
    Ready_worker가 수행하는 task

    :param int push_id: ops_push table의 primary key
    :param str redis_token_key: ios_device_push_token을 꺼내기 위한 redis key
    """
    print("{} : send IOS".format(self.request.id))

    # 현재 task의 state를 'sending'으로 바꿈
    push_task = Session.query(OpsPushTask).filter(
        OpsPushTask.task_uuid == self.request.id).one()
    push_task.state = 'sending'
    Session.commit()

    # 현재 task에 해당하는 OpsPush 가져오기
    push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()

    # OpsPush와 매칭되는 OpsPushContent 찾기
    try:
        push_content = Session.query(OpsPushContent). \
            filter(OpsPushContent.id == push.ops_push_content_id).one()
    except NoResultFound:
        push_task.state = 'failed'
        push.state = 'failed'
        Session.commit()
        raise

    Session.close()

    # Redis에서 ios_device push_token을 꺼내 List()로 만듬
    unpickled = pickle.loads(redis_session.get(redis_token_key))
    device_token_list = [x[1] for x in unpickled['user_set']]

    push_ios_user(
        device_id_list=device_token_list,
        title=push_content.title,
        body=push_content.message,
        url=push_content.url,
        img_url=push_content.img_url,
        sound=push_content.sound,
        push_id=push_id
    )
    teardown.apply_async(queue='finish',
                         kwargs={'push_id': push_id,
                                 'task_uuid': self.request.id
                                 })


@app.task(bind=True)
def send_android(self, push_id, redis_token_key):
    """
    Ready_worker가 수행하는 task

    :param int push_id: ops_push table의 primary key
    :param str redis_token_key: android_device_push_token을 꺼내기 위한 redis key
    """
    print("{} : send android".format(self.request.id))

    # 현재 task의 state를 'sending'으로 바꿈
    push_task = Session.query(OpsPushTask).filter(
        OpsPushTask.task_uuid == self.request.id).one()
    push_task.state = 'sending'
    Session.commit()

    # 현재 task에 해당하는 OpsPush 가져오기
    push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()

    # OpsPush와 매칭되는 OpsPushContent 찾기
    try:
        push_content = Session.query(OpsPushContent). \
            filter(OpsPushContent.id == push.ops_push_content_id).one()
    except NoResultFound:
        push_task.state = 'failed'
        push.state = 'failed'
        Session.commit()
        raise

    Session.close()

    # Redis에서 android_device push_token을 꺼내 List()로 만듬
    unpickled = pickle.loads(redis_session.get(redis_token_key))
    device_token_list = [x[1] for x in unpickled['user_set']]

    # OpsPushContent 내용으로 Push 전송하기
    push_android_user(
        device_id_list=device_token_list,
        title=push_content.title,
        body=push_content.message,
        url=push_content.url,
        img_url=push_content.img_url,
        sound=push_content.sound,
        is_popup=push_content.is_popup,
        push_id=push_id
    )

    teardown.apply_async(queue='finish',
                         kwargs={'push_id': push_id,
                                 'task_uuid': self.request.id
                                 })


@app.task(bind=True)
def teardown(self, push_id, task_uuid):
    """
    finish worker가 수행하는 Task

    Push 발송 완료한 task를 정리합니다.
    1. 해당 task의 상태를 update ('finished' or 'failed')합니다.
    2. 해당 task가 속한 OpsPush.state를 update
        ('sending', 'finished' or 'failed')합니다.

    :param int push_id: ops_push table의 primary key
    :param str task_uuid: celery task uuid
    """

    # 1. OpsPushTask update state = finish from OpsPushTask
    try:
        push_task = Session.query(OpsPushTask).filter(
            OpsPushTask.task_uuid == task_uuid).one()
    except NoResultFound:
        print("task_uuid({}) is not found".format(task_uuid))
        Session.close()
        return False

    if push_task.state == 'sending':
        push_task.state = 'finished'
    else:
        push_task.state = 'failed'

    try:
        Session.commit()
    except Exception:
        Session.rollback()

    push_tasks = Session.query(OpsPushTask).filter(
        OpsPushTask.ops_push_id == push_id).all()
    tasks_count = len(push_tasks)
    finished_count = 0
    is_failed = False
    for x in push_tasks:
        if x.state == 'finished':
            finished_count = finished_count + 1
        elif x.state == 'failed':
            is_failed = True

    # 2. OpsPush.id == push_id에 해당되는 sub task의 상태에 따라 OpsPush.state를 변경
    #    if   finished_count == 0           : OpsPush.state == 'ready'
    #    elif finished_count <  task_count  : OpsPush.state == 'sending'
    #    elif finished_count == task_count  : OpsPush.state == 'finished'
    #    else(finished_count >  task_count) : Error
    push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()

    if finished_count < tasks_count:
        push.state = 'sending'
    elif finished_count == tasks_count:
        push.state = 'finished'
    else:
        print("Error")

    # 적어도 1개의 push_task.state == 'failed'면, push.state = 'failed'
    if is_failed:
        push.state = 'failed'

    print("push({}).state = {}".format(push_id, push.state))

    Session.commit()
    Session.close()


@app.task(bind=True)
def test_push_send(self, push_id):
    """
    test-worker가 수행하는 task

    API /push/{push_id}/test POST에서 호출
    push.state = 'created' or 'tested'에서만 동작
    push.id == {push_id}인 push를 test user에게 발송합니다.

    OpsPushTestReceiver에 등록된 모든 test user에 대해 발송합니다.
    Test Push를 발송하면, push.id == {push_id}인 push.state를 'tested'로 변경합니다.

    :param int push_id: ops_push table의 primary key
    """
    push = Session.query(OpsPush).filter(OpsPush.id == push_id).one()

    push_content = Session.query(OpsPushContent).\
        filter(OpsPushContent.id == push.ops_push_content_id).one()

    # push_id.state == 'created', 'tested' 아니면 task 파기
    if push.state not in ['created', 'tested', 'spawned']:
        print("push({id}).state = {state} (must be 'created', 'tested' or 'spawned')".
              format(id=push_id, state=push.state))
        Session.close()
        return False

    # DB에서 test user_set 가져옴
    test_users = Session.query(OpsPushTestReceiver.user_id).all()

    # user_id -> ('user_id', 'push_code')
    r = userid_to_token_extraction(test_users)
    android_userset = set(r['android'])
    ios_userset = set(r['ios'])

    if len(android_userset):
        android_device_list = [x[1] for x in android_userset]
        push_android_user(
            device_id_list=android_device_list,
            title=push_content.title,
            body=push_content.message,
            url=push_content.url,
            img_url=push_content.img_url,
            sound=push_content.sound,
            is_popup=push_content.is_popup
        )

    if len(ios_userset):
        ios_device_list = [x[1] for x in ios_userset]
        push_ios_user(
            device_id_list=ios_device_list,
            title=push_content.title,
            body=push_content.message,
            url=push_content.url,
            img_url=push_content.img_url,
            sound=push_content.sound
        )

    # push_id.state = tested 로 변경
    if push.state == 'spawned':
        push.state = 'spawned'
    else:
        push.state = 'tested'

    Session.commit()
    Session.close()


@app.task(bind=True)
def resume_push(self, push_id):
    """
    resume-worker가 수행하는
    일시정지 되어 있던 push 를 다시 시작한다.

    선택된 push의 하위 task 중 상태가 'paused'인 것들만
    다시 redis 에서 추출하여 send 해준다.

    :param int push_id: ops_push table의 primary key
    """
    push = OpsPush.read_one(Session, id=push_id)

    if push.state != 'paused':
        # push 의 상태가 'paused' 일때만 'resume' 이 가능하다.
        print("push가 'paused' 상태가 아닙니다.")
        Session.close()
        return False

    push_task_list = Session.query(OpsPushTask).filter(
        OpsPushTask.ops_push_id == push_id) \
        .filter(OpsPushTask.state == 'paused').all()

    if not push_task_list:
        print("push task 가 존재하지 않습니다.")
        Session.close()
        return False

    # Celery uuid Pool
    celery_task_pool = []
    for x in range(len(push_task_list)):
        celery_task_pool.append(uuid())

    old_task_uuid_list = []
    for push_task, preserved_uuid in zip(push_task_list, celery_task_pool):
        old_task_uuid_list.append(push_task.task_uuid)
        push_task.task_uuid = preserved_uuid
        push_task.state = 'ready'

    push.state = 'sending'

    Session.commit()

    now = datetime.now(tz=timezone('Asia/Seoul'))

    # 첫 eta 를 만들어 준다.
    if push.eta > now:
        android_eta = ios_eta = push.eta
    else:
        # 지금으로 부터 다음 MINUTE_INTERVAL 배수 (분)을 찾아 준다.
        minute_padding = MINUTE_INTERVAL - (now.minute % MINUTE_INTERVAL)
        android_eta = ios_eta = now.replace(second=0, microsecond=0)\
            + timedelta(minutes=minute_padding)

    # redis 만료 시간
    a_week_after = int(timedelta(days=7).total_seconds())

    for old_uuid, preserved_uuid in zip(old_task_uuid_list, celery_task_pool):
        pickled = redis_session.get(old_uuid)

        if not pickled:
            continue

        # 데이터 형식은 아래와 같다.
        # {'os':'android or ios', 'user_set':[('user_id', 'device_id')]}
        data = pickle.loads(pickled)

        try:
            device_list = [x[1] for x in data['user_set']]
        except KeyError:
            continue

        if not device_list:
            continue

        if data['os'] == 'android':
            android_expire = android_eta + timedelta(
                minutes=MINUTE_INTERVAL)

            if android_expire >= push.expire:
                android_expire = push.expire

            send_android.apply_async(
                queue='ready',
                task_id=preserved_uuid,
                kwargs={
                    'push_id': push_id,
                    'android_device_list': device_list},
                eta=android_eta,
                expires=android_expire
            )

            android_eta = android_eta + timedelta(minutes=MINUTE_INTERVAL)

        else:
            ios_expire = ios_eta + timedelta(
                minutes=MINUTE_INTERVAL)

            if ios_expire >= push.expire:
                ios_expire = push.expire

            send_ios.apply_async(
                queue='ready',
                task_id=preserved_uuid,
                kwargs={
                    'push_id': push_id,
                    'ios_device_list': device_list},
                eta=ios_eta,
                expires=ios_expire
            )

            ios_eta = ios_eta + timedelta(minutes=MINUTE_INTERVAL)

        redis_session.set(name=preserved_uuid,
                          value=pickled,
                          ex=a_week_after,
                          nx=True)
        redis_session.delete(old_uuid)

    Session.close()


@app.task(bind=True, base=EstimateExtractionBase)
def estimate_extraction(self, push_user_extraction_id):
    """
    OPS_PUSH_USER_EXTRCATION.id 로 day_interval, date_offset를 기반으로
    해당 기간동안 서비스를 이용한 사람의 user_id를 추출한다.

    :param int push_user_extraction_id: ops_push_user_extraction table의 primary key
    """
    # Acquire the estimate lock
    lock_key = "OpsPushUserExtraction.{}.estimate.lock".format(
        push_user_extraction_id)
    redis_session.set(name=lock_key, value="locked")

    # get Date from DB
    try:
        Session.query(OpsPushUserExtraction).filter(
            OpsPushUserExtraction.id == push_user_extraction_id).one()

    except NoResultFound:
        print("API에서 NoResultFound가 처리되지 않음.")
        raise

    except MultipleResultsFound:
        print("API에서 extraction_id가 unique함을 처리하지 않았음")
        raise

    Session.close()

    estimated = get_user_set_by(push_user_extraction_id)

    count = len(estimated['android']) + len(estimated['ios'])

    # SETEX PushUserExtraction.id, 밤 12시, count
    now = datetime.now(tz=timezone('Asia/Seoul'))
    midnight = now.replace(hour=23, minute=59, second=59, microsecond=0)
    expired = int((midnight - now).total_seconds())
    redis_session.set(
        name="OpsPushUserExtraction.{}.estimate".format(
            push_user_extraction_id),
        value=count,
        ex=expired,
        nx=True)

    # Release a estimate lock
    redis_session.delete(lock_key)
    return count


def get_user_set_by(user_extraction_id):
    """
    Push Token을 추출하기 위한 여러가지 방법들을 모아둔 함수.

    UserExtraction.type에 맞는 추출 로직을 사용해 push token을 추출하고 dict 결과를 반환함.

    :param int push_user_extraction_id: ops_push_user_extraction table의 primary key
    :return: (dict) {'android': android_users, 'ios': ios_users}
    """
    user_extraction = Session.query(
        OpsPushUserExtraction.type,
        OpsPushUserExtraction.value,
        OpsPushUserExtraction.day_interval,
        OpsPushUserExtraction.date_offset,
        OpsPushUserExtraction.extract_count,
        OpsPushUserExtraction.os
    ).filter(
        OpsPushUserExtraction.id == user_extraction_id
    ).one()

    Session.close()

    # user_extraction.type에 따라 추출 방법 선택
    ios_users = set()
    android_users = set()

    if user_extraction.type == 'ordinary':
        # 일반 유저 추출
        r = ordinary_user_extraction(day_interval=user_extraction.day_interval,
                                     date_offset=user_extraction.date_offset)
        android_users = r['android']
        ios_users = r['ios']

    elif user_extraction.type == 'dormant':
        r = ordinary_user_extraction(day_interval=user_extraction.day_interval,
                                     date_offset=user_extraction.date_offset)
        android_users = r['android']
        ios_users = r['ios']

    elif user_extraction.type == 'moaweek':
        keywords = user_extraction.value.split(', ')
        r = keywords_user_extraction(keywords=keywords)
        android_users = set(r['android'])
        ios_users = set(r['ios'])

    elif user_extraction.type == 'search_personalize':
        pass

    elif user_extraction.type == 'event':
        # With evan, Comma seperated value
        # event_id,start_date,end_date
        event_id, start_date, end_date = user_extraction.value.split(",")
        r = event_user_extraction(
            event_id=event_id,
            from_date=start_date,
            to_date=end_date)
        android_users = set(r['android'])
        ios_users = set(r['ios'])

    elif user_extraction.type == 'test':
        # OpsPushTestReceiver에 등록된 사용자 추출
        test_users = Session.query(OpsPushTestReceiver.user_id).all()

        # user_id -> ('user_id', 'push_code')
        r = test_user_extraction(test_users)
        android_users = set(r['android'])
        ios_users = set(r['ios'])

    elif user_extraction.type == 'csv':
        # resources/user_id_csv로 서버 캐시에 저장된 CSV 파일에서 추출
        if redis_session.exists(user_extraction.value):
            r = csv_user_extraction(user_extraction.value)
            android_users = set(r['android'])
            ios_users = set(r['ios'])
        else:
            android_users = set()
            ios_users = set()

    else:
        """ Not implemented """
        return None
    return {'android': android_users, 'ios': ios_users}
