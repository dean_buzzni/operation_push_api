import datetime

from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import Interval
from sqlalchemy import Text
from sqlalchemy.sql import func

from sources.database import Base
from sources.models.base import ModelMixin


class OpsPushUserExtraction(Base, ModelMixin):
    __tablename__ = 'ops_push_user_extraction'

    id = Column(BigInteger, primary_key=True)
    os = Column(Text, nullable=False)
    date_offset = Column(DateTime, default=None)
    day_interval = Column(Interval, nullable=False)
    extract_count = Column(Integer, nullable=False, default=-1)
    type = Column(Text, nullable=False, default='ordinary')
    value = Column(Text)
    created_at = Column(DateTime(timezone=True),
                        nullable=False,
                        default=func.now()
                        )
    last_modified = Column(DateTime(timezone=True),
                           nullable=False,
                           default=func.now(),
                           onupdate=func.now()
                           )

    def to_dict(self):
        """
        만들어진 orm 에서 값들을 dict 로 만들어서
        전달해 준다.
        """
        push_user_extraction = {}

        for column in OpsPushUserExtraction.get_columns():
            value = self.__dict__.get(column)

            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y%m%d%H%M%S")
            if isinstance(value, datetime.timedelta):
                value = value.days

            push_user_extraction[column] = value

        return push_user_extraction
