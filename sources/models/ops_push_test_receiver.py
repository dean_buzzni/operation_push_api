import datetime

from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Text
from sqlalchemy.sql import func

from sources.database import Base
from sources.models.base import ModelMixin


class OpsPushTestReceiver(Base, ModelMixin):
    __tablename__ = 'ops_push_test_receiver'
    id = Column(BigInteger, primary_key=True)
    created_at = Column(DateTime(timezone=True),
                        nullable=False,
                        default=func.now()
                        )
    last_modified = Column(DateTime(timezone=True),
                           nullable=False,
                           default=func.now(),
                           onupdate=func.now()
                           )

    user_id = Column(Text, nullable=False)
    tag = Column(Text)

    def to_dict(self):
        """
        만들어진 orm 에서 값들을 dict 로 만들어서
        전달해 준다.
        """
        testreceiver = {}
        for column in OpsPushTestReceiver.get_columns():
            value = self.__dict__.get(column)
            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y%m%d%H%M%S")
            testreceiver[column] = value
        return testreceiver
