import datetime

from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Text
from sqlalchemy.sql import func

from sources.database import Base
from sources.models.base import ModelMixin


class OpsPushTask(Base, ModelMixin):
    __tablename__ = 'ops_push_task'
    id = Column(BigInteger, primary_key=True)
    created_at = Column(DateTime(timezone=True),
                        nullable=False,
                        default=func.now()
                        )
    last_modified = Column(DateTime(timezone=True),
                           nullable=False,
                           default=func.now(),
                           onupdate=func.now()
                           )
    # Foreign Keys
    ops_push_id = Column(BigInteger, nullable=False)

    # systemical variables
    task_uuid = Column(BigInteger, nullable=False)
    state = Column(Text)

    def to_dict(self):
        """
        만들어진 orm 에서 값들을 dict 로 만들어서
        전달해 준다.
        """
        push = {}
        for column in OpsPushTask.get_columns():
            value = self.__dict__.get(column)
            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y%m%d%H%M%S")
            push[column] = value
        return push
