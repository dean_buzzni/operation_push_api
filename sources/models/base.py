

class ModelMixin(object):
    """Model 들에서 쓰는 공통적인 Methods 를 모아 놓는 곳이다.

    공통으로 쓰일수 있는게 있다면 여기에 추가해서 사용하자.
    """

    @classmethod
    def read_one(cls, session, **kwargs):
        """한개만 읽어서 반환한다.

        여러개의 Args 를 전달 key=value 형대로 전달 받아서
        and 조건으로 해서 하나의 값만 반환 해준다.

        (예: SomeModel.read_one(Session, id=4, title='title')
        """
        query = session.query(cls)
        if kwargs:
            for key, value in kwargs.items():
                query = query.filter(getattr(cls, key) == value)
        return query.one()

    @classmethod
    def get_columns(cls):
        """ 기존 columns 를 가져온다."""
        return cls.__table__.columns.keys()

    @classmethod
    def is_exists(cls, session, **kwargs):
        """해당 조건의 데이터가 존재하는지 존재 여부를 반환한다.

        Return: boolean
        """
        query = session.query(cls)
        if kwargs:
            for key, value in kwargs.items():
                query = query.filter(getattr(cls, key) == value)
        return session.query(query.exists()).scalar()
