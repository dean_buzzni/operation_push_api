import datetime

from sqlalchemy import BigInteger
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Text
from sqlalchemy.sql import func

from sources.database import Base
from sources.models.base import ModelMixin


class OpsPushContent(Base, ModelMixin):
    __tablename__ = 'ops_push_content'

    id = Column(BigInteger, primary_key=True)
    title = Column(Text, nullable=False)
    message = Column(Text, default=None)
    img_url = Column(Text, default=None)
    url = Column(Text, default=None)
    tag = Column(Text, default=None)
    is_ad = Column(Boolean, default=False)
    sound = Column(Text, default="found_item")
    is_popup = Column(Boolean, default=True)
    type = Column(Text, nullable=False)
    created_at = Column(DateTime(timezone=True),
                        nullable=False,
                        default=func.now()
                        )
    last_modified = Column(DateTime(timezone=True),
                           nullable=False,
                           default=func.now(),
                           onupdate=func.now()
                           )

    def to_dict(self):
        """
        만들어진 orm 에서 값들을 dict 로 만들어서
        전달해 준다.
        """
        push_content = {}

        for column in OpsPushContent.get_columns():
            value = self.__dict__.get(column)

            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y%m%d%H%M%S")

            push_content[column] = value

        return push_content
