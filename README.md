# Operation Push

Operation Push (OpsPush)는 FCM, APNS를 사용해 홈쇼핑모아 사용자들에게 푸시를 전송합니다.

Docker file, Docker images, Kubernetes 설정 파일을 제공합니다.

## Concept

### Overview

OpsPush는 3가지 구성요소를 갖습니다.

1. interface; 외부와 커뮤니케이션 하기 위한 HTTP REST API
	- OpsPush에서 사용하는 Database의 Table에 대응하는 Resources API
	- OpsPush Core를 구성하는 Celery Task를 제어하는 Control API
2. core; 실제 푸시를 발송하고 푸시 관련 정보를 제어하는 Logic
	- Python celery task; 푸시 스케쥴과 정책에 따라 FCM, APNS 서버로 푸시 요청을 보내는 Task
		- (Kubernetes Job으로 대체하려고 함)
	- Kubernetes Cron job; 주기적으로 동작하는 루틴
		- Push Task의 integrity를 위한 동작 (Deadline이 넘은 task 정리와 DB 동기화)
		- Memory 사용량 최적화
3. Storage
	- RDB (postgres)
	- Cache (Redis)
	- MessageQueue (RabbitMQ)


### Interface (API)

Docker Containter의 Port 8000을 사용해 외부와 커뮤니케이션 합니다.

API명세는 위키를 참조해주세요.

https://buzzni.atlassian.net/wiki/spaces/buzzni/pages/528547925/Push+admin+API


### Core (Celery worker)

OpsPush의 Core는 다수의 Celery task를 수행합니다.

- ** Tasks **
	- 특정 시간에 Push가 전송되도록 ETA 설정
	- 다른 Push와 겹치지 않도록 Task Expire 관리
	- Push 대상자 추출
	- Celery Task와 Database 동기화
	- 테스트 Push 전송
	- 특정 Push Taks의 일시정지/재시작
	- 무결성을 해치는 Celery Task, Database 관리 (cron job)
- ** Queues **
	- Spawn Queue;	Push Task 생성과 유저 추출 Task를 위한 Queue
	- Ready Queue; 	Push 발송 Task를 위한 Queue
	- Finish Queue;	Celery Task와 Database 동기 처리를 위한 Queue
	- Resume Queue;	일시정지/재시작 Task를 위한 Queue
	- Test Queue;	Test Push 발송 Task를 위한 Queue


Task를 수행하는 Worker와 Task가 저장되는 Queue가 분리되어있습니다.

필요에따라 Worker 수를 조절해 Queue에 쌓이는 Task양을 조절할 수 있습니다.



### Storage

OpsPush는 3종류의 데이터를 갖고, 종류에 따라 다른 Application에 저장됩니다.

- **지속성 데이터** (postgres)
	- Push Contents (ops_push_content Table)
	- 유저 추출 템플릿 (ops_push_user_extraction Table)
	- Push 템플릿 (ops_push Table)
	- PushTask 정보 (ops_push_task table)
	- 테스트 유저 정보 (ops_push_test_receiver Table)
- **일시적 데이터** (Redis)
	- Push 대상자 정보를 포함한 CSV file
	- Push Task의 pause/resume을 위한 임시 데이터
	- Celery Task 동기를 위한 Semaphore
	- 사용자 추출 추정값 (실제 발송에는 사용되지 않음)
	- 실제 발송에 사용된 user_id csv (사용자에게 정보 제공 목적)
- **Celery Task Message** (RabbitMQ)
	- Celery Task Info
	- ETA, Expire
    - uuid
    - Push 발송을 위한 정보의 Redis Key

## File Hierarchy

```shell
/dockerfiles		# Docker image 정의를 위한 dockerfile들
	...
/kubernetes			# Kubernetes deployments, services, secrets를 정의한 yaml files
	...
/references			# 실제 동작, 배포와 무관한 파일		
	sample.csv				# user_id가 포함된 csv 예시파일
	insert_sample_data.py	# 임의의 OpsPush(Contents, Userextraction, Push) API 요청하는 script
/sources			# 핵심 코드
	/cron					# kubernetes에서 주기적으로 실행될 jobs
	/models					# OpsPush Database를 반영한 sqlalchemy model
	/push					# FCM, APSN로 Post requests 요청하는 루틴
	/resources				# 외부와 통신하는 interface 정의 (REST API)
	/schemas				# REST API로 들어오는 Request를 제한하는 json schemas
	app.py					# Falcon app
	consumer.py				# Celery Task 정의
	database.py				# Falcon, Celery Task에서 공용으로 사용하는 DB connection 정보
/tests				# pytest
create_table.sql	# postsql container를 생성할 때 실행되는 sql
docker-compose.yml	# docker-compose 정의
README.md
requirements.txt	# pip requirments
```



# Support

**Maintainer**

marco (marco@buzzni.com)

**Contributor**

zunik (zunik@buzzmi.com)

jacob (jacob@buzzni.com)
