.. operation_push_api documentation master file, created by
   sphinx-quickstart on Mon Oct 29 18:27:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to operation_push_api's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: operation_push_api

   sources/push
   sources/resources
   sources/consumer
   sources/extraction