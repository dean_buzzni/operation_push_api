import datetime
import random
import requests
import secrets

from pytz import timezone
from string import ascii_letters

# test server full_url
SERVER_URL = "http://192.168.0.32:8000"
LOCAL_SERVER = "http://localhost:8000"


def random_text(length=10):
    return ''.join(random.sample(ascii_letters, length))


def random_os():
    return random.sample(['android', 'ios', 'both'], 1)[0]


def random_url(postfix=None):
    return secrets.token_urlsafe() + postfix


def random_interval(limit=-1):
    return random.randint(limit, -1)


def random_boolean():
    return random.sample([True, False], 1)[0]


def random_date():
    year = 2018
    month = random.choice(range(1, 13))
    day = random.choice(range(1, 29))
    hour = random.choice(range(9, 19))
    minute = random.choice([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55])
    some_day = datetime.datetime(year=year,
                                 month=month,
                                 day=day,
                                 hour=hour,
                                 minute=minute,
                                 second=0,
                                 microsecond=0,
                                 tzinfo=timezone('Asia/Seoul')
                                 )
    return some_day


def date_to_bts(datetime):
    return datetime.strftime("%Y%m%d%H%M%S")


def sample_user_extraction(url=SERVER_URL):
    post_url = url + '/userextractions'
    json_body = {
        "os": random_os(),
        "day_interval": random_interval(limit=-7),
        "extract_count": -1,
        "type": 'test'
    }
    send_a_post(url=post_url, body=json_body)


def sample_push_contents(url=SERVER_URL):
    post_url = url + '/pushcontents'
    json_body = {
        "title": random_text(),  # 필수 필드
        "message": random_text(),
        "url": random_url(postfix='.com'),
        "img_url": random_url(postfix='.png'),
        # ['ordinary', 'moaweek', 'search_personalized' 중 하나]
        "type": 'ordinary',
        "tag": random_text(4),
        "is_ad": random_boolean()}
    send_a_post(url=post_url, body=json_body)


def sample_push(url=SERVER_URL):
    post_url = url + '/pushes'
    some_datetime = random_date()
    after_5min = some_datetime + datetime.timedelta(minutes=5)
    json_body = {
        "eta": date_to_bts(some_datetime),
        "expire": date_to_bts(after_5min),
        "ops_push_content_id": random.choice(range(1, 50)),
        "ops_push_user_extraction_id": random.choice(range(1, 50)),
        "push_id_to_deny": []
    }
    send_a_post(url=post_url, body=json_body)


def send_a_post(url, body):
    header = {
        'Content-Type': 'application/json'
    }
    r = requests.post(url=url,
                      headers=header,
                      json=body)
    print(r.text)


def main():
    for x in range(50):
        # sample_push_contents()
        # sample_user_extraction()
        # sample_push()
        sample_push_contents(LOCAL_SERVER)
        sample_user_extraction(LOCAL_SERVER)
        sample_push(LOCAL_SERVER)


if __name__ == '__main__':
    main()
