import csv


def parse_this(stream):
    user_set = set()
    column_names = "num,xid"

    splited_stream = stream.split('\n')
    if splited_stream[0] != column_names:
        print("invalid CSV")
    else:
        for line in splited_stream[1:]:
            user_id = line.split(',')[1]
            user_set.add(user_id)
    return user_set


def main():
    r = parse_this("num,xid\n1,xTUJjrcEbl\n2,2V8UGv8uoE\n3,MnVgttTwtK")
    print(r)


if __name__ == '__main__':
    main()
