from falcon import testing
import pytest

from sources.app import app
from sources.consumer import app as real_celery_app


@pytest.fixture
def client():
    # Assume the hypothetical `myapp` package has a function called
    # `create()` to initialize and return a `falcon.API` instance.
    return testing.TestClient(app)


@pytest.fixture(scope="module")
def celery_app(request):
    real_celery_app.conf.task_always_eager = True
    return real_celery_app
