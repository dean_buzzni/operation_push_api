"""
 Model 관련 테스트 파일이다.
 하나의 Model 에 하나의 class 를 사용한다.
"""
from unittest.mock import patch

import pytest
from sqlalchemy.orm.exc import NoResultFound

from sources.models.ops_push_contents import OpsPushContent
from sources.database import Session


class TestOpsPushContents(object):
    @patch.object(Session, 'query')
    def test_read_one_success(self, mock_query):
        """
        id 를 통해 push content 를 찾을때
        one 까지 잘 호출이 되는지 확인한다.
        """
        OpsPushContent.read_one(Session, id=10)
        mock_query.assert_called_once_with(OpsPushContent)
        mock_query().filter().one.assert_called_once()

    @patch.object(Session, 'query')
    def test_read_one_result_found_failure(self, mock_query):
        """
        특정 아이디를 가진 push content 를 찾으려 할때
        NoResultFound 가 잘 반환 되는지 확인
        """
        mock_query().filter().one.side_effect = NoResultFound
        with pytest.raises(NoResultFound):
            OpsPushContent.read_one(Session, id=99)

    def test_to_dict_success(self):
        """
        to_dict 가 잘 작동하는지 확인 한다.
        객체를 만들어 주고 to_dict 을 통해 값을 뽑아서
        일치하는 값이 나오는지 확인한다.
        """
        columns = OpsPushContent.get_columns()

        push_content_dict = {'title': 'title', 'message': 'message', 'img_url': 'http://test.com/test.jpg',
                             'url': 'http://test.com', 'tag': '생활', 'type': 'ordinary'}
        test_content = OpsPushContent(**push_content_dict)

        for column in columns:
            if column not in push_content_dict:
                push_content_dict[column] = None

        assert push_content_dict == test_content.to_dict()