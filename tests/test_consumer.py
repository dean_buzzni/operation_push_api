import pickle
from datetime import datetime, timedelta
from unittest.mock import patch

from pytz import timezone

from sources.models.ops_push import OpsPush
from sources.models.ops_push_task import OpsPushTask
from sources.models.ops_push_contents import OpsPushContent
from sources.models.ops_push_user_extraction import OpsPushUserExtraction
from sources.database import Session, redis_session

# push 의 상태 전체 리스트
PUSH_STATE_LIST = {'created', 'tested', 'spawned', 'ready', 'sending',
                   'finished', 'failed', 'paused'}


@patch("sources.consumer.push_ios_user")
@patch("sources.consumer.push_android_user")
@patch("sources.consumer.userid_to_token_extraction")
@patch.object(Session, 'commit')
@patch.object(Session, 'query')
class TestTestPushSend(object):
    """ test_push_send Task 테스트 """
    ALLOW_PUSH_STATE = {'created', 'tested', 'spawned'}

    def test_both_success(self, mock_query, mock_commit, mock_extraction,
                          mock_push_android, mock_push_ios, celery_app):
        """ allow 된 모든 state 를 테스트 한다.

        android, ios 유저 다 넘겨서 둘다 전송 함 수를 정상적으로 호출하는지 확인한다.
        """
        test_push_send = celery_app.tasks['sources.consumer.test_push_send']

        for state in self.ALLOW_PUSH_STATE:
            mock_query().filter().one.side_effect = \
                [OpsPush(state=state), OpsPushContent(title='test')]

            mock_extraction.return_value = \
                {'android': [('userid', 'devicecode')], 'ios': [
                    ('userid', 'devicecode')]}
            result = test_push_send.apply_async(kwargs={'push_id': 1})

        assert mock_commit.call_count == len(self.ALLOW_PUSH_STATE),\
            "{}번 성공해야 하는데 {}번만 성공했습니다.".\
            format(len(self.ALLOW_PUSH_STATE), mock_commit.call_count)
        # commit 으로 성공횟수를 체크했기 때문에 하단에서는 실행된지만 확인한다.
        assert mock_push_android.called
        assert mock_push_ios.called
        assert result.get() is None

    def test_android_success(self, mock_query, mock_commit, mock_extraction,
                             mock_push_android, mock_push_ios, celery_app):
        """안드로이드 유저만 나오면 ios 에 안 가는지 확인하고 전송되는 args 를 확인한다.

        push_android 를 실행했을때 정상적으로 args 가 들어가는지 확인한다.
        """
        push_content = OpsPushContent(
            title='test',
            message='message',
            url='http://test.com',
            img_url='test.jpg',
            sound='off',
            is_popup=False
        )
        mock_query().filter().one.side_effect = \
            [OpsPush(state='created'),
             push_content]

        test_user_set = \
            {'android': [('userid', 'devicecode')], 'ios': []}
        mock_extraction.return_value = test_user_set

        test_push_send = celery_app.tasks['sources.consumer.test_push_send']
        result = test_push_send.apply_async(kwargs={'push_id': 1})

        mock_push_android.assert_called_with(
            device_id_list=[x[1] for x in set(test_user_set['android'])],
            title=push_content.title,
            body=push_content.message,
            url=push_content.url,
            img_url=push_content.img_url,
            sound=push_content.sound,
            is_popup=push_content.is_popup
        )
        # ios 가 넘어가지 않았기 때문에 push_ios 는 실행되지 않는다.
        mock_push_ios.assert_not_called()
        assert result.get() is None

    def test_not_allowed_state_failure(
            self, mock_query, mock_commit, mock_extraction,
            mock_push_android, mock_push_ios, celery_app):
        """ 허용되지 않은 state 이 잘 실패하는지 확인한다."""
        not_allow_state = PUSH_STATE_LIST - self.ALLOW_PUSH_STATE
        test_push_send = celery_app.tasks['sources.consumer.test_push_send']

        for state in not_allow_state:
            mock_query().filter().one.side_effect = \
                [OpsPush(state=state), OpsPushContent(title='test')]
            result = test_push_send.apply_async(kwargs={'push_id': 1})

        mock_commit.assert_not_called()
        assert result.get() is False


@patch.object(redis_session, 'delete')
@patch.object(redis_session, 'set')
@patch.object(redis_session, 'get')
@patch("sources.consumer.send_android.apply_async")
@patch("sources.consumer.send_ios.apply_async")
@patch.object(Session, 'query')
@patch.object(OpsPush, 'read_one')
class TestResumePush(object):
    """ resume_push Task 를 테스트 한다. """
    push_eta = datetime.now(tz=timezone('Asia/Seoul')) + timedelta(minutes=5)
    push_late_eta = datetime.now(tz=timezone('Asia/Seoul')) - timedelta(
        minutes=5)
    push_expire = push_eta + timedelta(minutes=20)

    def test_success(self, mock_read_one, mock_query, mock_send_android,
                     mock_send_ios, mock_redis_get, mock_redis_set,
                     mock_redis_delete, celery_app):
        """ resume 성공을 테스트한다.

        eta 시간이 잘 만들어 지는지 확인하고 전체적 성공을 테스트 한다.
        """
        resume_push = celery_app.tasks['sources.consumer.resume_push']

        mock_read_one.return_value = \
            OpsPush(state='paused', eta=self.push_late_eta,
                    expire=self.push_expire)

        ops_push_task_list = []
        task_count = 4  # task 개수 짝수로만 한다고 가정하자 (android ios) 가 매칭되기 때문
        for i in range(task_count):
            task_uuid = "testuuid" + str(i)
            ops_push_task_list.\
                append(OpsPushTask(task_uuid=task_uuid))

        mock_query().filter().filter().all.return_value = ops_push_task_list
        mock_redis_get.side_effect = \
            self.get_pickled_tmp_data(task_count)

        result = resume_push.apply_async(kwargs={'push_id': 1})

        now = datetime.now(tz=timezone('Asia/Seoul'))
        minute_interval = 5
        minute_padding = minute_interval - (now.minute % minute_interval)

        if (task_count / 2) >= 2:
            minute_padding = minute_padding + (task_count / 2 - 1) * 5
        # 각 android, ios 에 대해서 마지막 eta 시간을 만든다.
        assert_eta = now.replace(second=0, microsecond=0) \
            + timedelta(minutes=minute_padding)

        # ios, android 각 마지막 호출 args 를 가져온다.
        android_last_args = list(mock_send_android.call_args)[1]
        ios_last_args = list(mock_send_ios.call_args)[1]

        assert assert_eta == android_last_args['eta']
        assert assert_eta == ios_last_args['eta']
        assert result.get() is None

    def test_not_allowed_state_failure(
            self, mock_read_one, mock_query, mock_send_android,
            mock_send_ios, mock_redis_get, mock_redis_set,
            mock_redis_delete, celery_app):
        """허용되지 않은 state 들이 정상적으로 실패하는지 확인한다. """
        not_allow_state = PUSH_STATE_LIST.copy()
        not_allow_state.remove('paused')

        resume_push = celery_app.tasks['sources.consumer.resume_push']

        for state in not_allow_state:
            mock_read_one.return_value = \
                OpsPush(state=state, eta=self.push_late_eta,
                        expire=self.push_expire)
            result = resume_push.apply_async(kwargs={'push_id': 1})

        mock_send_android.assert_not_called()
        mock_send_ios.assert_not_called()
        assert result.get() is False

    def get_pickled_tmp_data(self, list_length=1):
        """ pickled 된 테스트 데이터를 반환한다.

        ios, android 가 한 번씩 번갈아 가면서 만들어진다.
        """
        device_toggle = False
        device_list = ['android', 'ios']
        pickled_list = []

        for i in range(list_length):
            user_set = {'os': device_list[int(device_toggle)],
                        'user_set': [('1111', 'deviceid')]}
            pickled_list.append(pickle.dumps(user_set))
            device_toggle = not device_toggle

        return pickled_list


@patch("sources.consumer.send_android.apply_async")
@patch("sources.consumer.send_ios.apply_async")
@patch("sources.consumer.get_user_set_by")
@patch.object(redis_session, 'set')
@patch.object(redis_session, 'get')
@patch.object(Session, 'commit')
@patch.object(Session, 'query')
class TestSpawn(object):
    push_eta = datetime.now(tz=timezone('Asia/Seoul')) + timedelta(minutes=5)
    push_expire = push_eta + timedelta(minutes=20)

    def test_success(self, mock_query, mock_commit, mock_redis_get,
                     mock_redis_set, mock_get_user_set_by, mock_send_android,
                     mock_send_ios, celery_app):
        """ spawn 에 성공한 경우"""
        spawn_push = celery_app.tasks['sources.consumer.spawn']

        ops_push = \
            OpsPush(state='spawned', eta=self.push_eta,
                    expire=self.push_expire)
        ops_push_user_extraction = \
            OpsPushUserExtraction(id=1, extract_count=-1, os='both',
                                  type='ordinary')

        mock_query().filter().one.side_effect = [
            ops_push,
            ops_push_user_extraction
        ]

        # 테스트 데이터를 android, ios 개수 만큼 만들어준다.
        mock_get_user_set_by.return_value = \
            self.get_user_extraction_tmp_data(
                android_length=3000, ios_length=300)
        mock_redis_get.side_effect = [
            False,  # OpsPush.{}.push_id_to_deny 를 get 하는 부분
        ]

        push_id = 1
        result = spawn_push.apply_async(kwargs={'push_id': push_id})

        for device_type in ['android', 'ios']:
            eta_check = self.push_eta
            mock_send = locals()['mock_send_' + device_type]
            # 실행당시 args 를 하나씩 꺼내서 제대로 되었는지 확인한다.
            for call_args in mock_send.call_args_list:
                call_args = tuple(call_args)[1]
                assert call_args['eta'] == eta_check
                assert call_args['expires'] <= self.push_expire,\
                    "task의 expire 가 push의 expire 를 넘을 수 없습니다."
                eta_check = eta_check + timedelta(minutes=5)

        assert result.get() == "push.{} spawned.".format(push_id)

    @patch.object(pickle, "dumps")
    def test_users_limit_success(
            self, mock_pickle_dumps, mock_query, mock_commit, mock_redis_get,
            mock_redis_set, mock_get_user_set_by, mock_send_android,
            mock_send_ios, celery_app):
        """ 추출된 유저중에서도 개수를 정해서 보내고 싶을때 잘 되는지 """
        spawn_push = celery_app.tasks['sources.consumer.spawn']
        extract_count = 100

        ops_push = \
            OpsPush(state='spawned', eta=self.push_eta,
                    expire=self.push_expire)
        ops_push_user_extraction = \
            OpsPushUserExtraction(id=1, extract_count=extract_count, os='both',
                                  type='ordinary')

        mock_query().filter().one.side_effect = [
            ops_push,
            ops_push_user_extraction
        ]

        mock_get_user_set_by.return_value = \
            self.get_user_extraction_tmp_data(android_length=3000,
                                              ios_length=300)
        mock_redis_get.side_effect = [
            False,  # OpsPush.{}.push_id_to_deny 를 get 하는 부분
        ]

        push_id = 1
        result = spawn_push.apply_async(kwargs={'push_id': push_id})

        # 중간 pickle dumps 하는 순간에 args 를 확인하여 원하는 extract 개수가 맞는지
        # 확인한다.
        after_extract_count = tuple(mock_pickle_dumps.call_args_list[0])[
            0][0]['count']
        assert extract_count == after_extract_count

        for device_type in ['android', 'ios']:
            eta_check = self.push_eta
            mock_send = locals()['mock_send_' + device_type]
            # 실행당시 args 를 하나씩 꺼내서 제대로 되었는지 확인한다.
            for call_args in list(mock_send.call_args_list):
                call_args = tuple(call_args)[1]
                assert call_args['eta'] == eta_check
                assert call_args['expires'] <= self.push_expire, \
                    "task의 expire 가 push의 expire 를 넘을 수 없습니다."
                eta_check = eta_check + timedelta(minutes=5)

        assert result.get() == "push.{} spawned.".format(push_id)

    def test_not_allowed_state_failure(
            self, mock_query, mock_commit, mock_redis_get, mock_redis_set,
            mock_get_user_set_by, mock_send_android, mock_send_ios, celery_app):
        """ 허용되지 않은 state 들이 정상적으로 실패하는지 확인"""
        not_allow_state = PUSH_STATE_LIST.copy()
        not_allow_state.remove('spawned')

        spawn_push = celery_app.tasks['sources.consumer.spawn']

        ops_push_user_extraction = \
            OpsPushUserExtraction(id=1, extract_count=-1, os='both',
                                  type='ordinary')

        for state in not_allow_state:
            ops_push = \
                OpsPush(state=state, eta=self.push_eta,
                        expire=self.push_expire)
            mock_query().filter().one.side_effect = [
                ops_push,
                ops_push_user_extraction
            ]
            result = spawn_push.apply_async(kwargs={'push_id': 1})

        mock_redis_get.assert_not_called()
        assert result.get() is False

    def get_user_extraction_tmp_data(self, android_length=0, ios_length=0):
        """ 테스트용 유저 추출 데이터를 만든다. """
        android_set = set()
        ios_set = set()

        for i in range(android_length):
            android_set.add(('userid' + str(i), 'deviceid' + str(i)))

        for i in range(ios_length):
            ios_set.add(('userid' + str(i), 'deviceid' + str(i)))

        return {'android': android_set, 'ios': ios_set}
