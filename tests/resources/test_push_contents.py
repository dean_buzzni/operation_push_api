import json
from unittest.mock import patch

import falcon
from sqlalchemy.orm.exc import NoResultFound

from sources.models.ops_push_contents import OpsPushContent
from sources.models.ops_push import OpsPush
from sources.database import Session

PUSH_CONTENTS_API_URL = '/pushcontents'


class TestPushContents(object):
    """ 단수의 Push Contents 에 대한 API 테스트 묶음 """

    @patch.object(OpsPushContent, 'read_one')
    def test_GET_success(self, mock_read_one, client):
        """Push Contents 의 Detail 정보가 잘 가져와지는지 테스트 한다."""
        mock_read_one.return_value = OpsPushContent(title='title', message='test message')

        response = client.simulate_get(PUSH_CONTENTS_API_URL + '/1')

        response_dict = json.loads(response.text)

        assert response_dict['message'] == "test message"
        assert response.status == falcon.HTTP_OK

    @patch.object(OpsPushContent, 'read_one')
    def test_GET_not_found_failure(self, mock_read_one, client):
        """ 없는 id 를 요청 했을때 """
        mock_read_one.side_effect = NoResultFound()

        response = client.simulate_get(PUSH_CONTENTS_API_URL + '/2')

        assert response.status == falcon.HTTP_NOT_FOUND

    @patch.object(Session, 'commit')
    @patch.object(OpsPushContent, 'read_one')
    def test_PATCH_success(self, mock_read_one, mock_commit, client):
        """Push Contents 가 잘 수정되는지 테스트 한다."""
        mock_read_one.return_value = OpsPushContent(title='title', message='test message')

        req_data = {"title": "Title", "message": "edit message", "url": "http://test.com/image.jpg"}

        response = client.simulate_patch(PUSH_CONTENTS_API_URL + '/1', body=json.dumps(req_data))

        mock_commit.assert_called_once()
        assert response.status == falcon.HTTP_OK

    @patch.object(Session, 'commit')
    @patch.object(OpsPushContent, 'read_one')
    def test_PATCH_not_found_failure(self, mock_read_one, mock_commit, client):
        """ 없는 id에 수정을 요청 했을때 """
        mock_read_one.side_effect = NoResultFound()

        req_data = {"title": "Title", "message": "edit message",
                    "url": "http://test.com/image.jpg"}

        response = client.simulate_patch(PUSH_CONTENTS_API_URL + '/2', body=json.dumps(req_data))

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_NOT_FOUND

    @patch.object(OpsPush, 'is_exists')
    @patch.object(Session, 'delete')
    @patch.object(Session, 'commit')
    @patch.object(OpsPushContent, 'read_one')
    def test_DELETE_success(self, mock_read_one, mock_commit, mock_delete, mock_exists, client):
        """ Push Contents 삭제 성공 테스트"""
        mock_read_one.return_value = OpsPushContent(title='title', message='test message')
        mock_exists.return_value = False

        response = client.simulate_delete(PUSH_CONTENTS_API_URL + '/1')

        mock_commit.assert_called_once()
        assert response.status == falcon.HTTP_NO_CONTENT

    @patch.object(OpsPush, 'is_exists')
    @patch.object(Session, 'delete')
    @patch.object(Session, 'commit')
    @patch.object(OpsPushContent, 'read_one')
    def test_DELETE_not_found_failure(self, mock_read_one, mock_commit,
                                      mock_delete, mock_exists, client):
        """ 없는 id 로 삭제하려 했을때 """
        mock_read_one.side_effect = NoResultFound()
        mock_exists.return_value = False

        response = client.simulate_delete(PUSH_CONTENTS_API_URL + '/2')

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_NOT_FOUND


class TestPushContentsCollection(object):

    @patch.object(Session, 'query')
    def test_GET_success(self, mock_query, client):
        """둘다 있을 때"""
        result = client.simulate_get(path=PUSH_CONTENTS_API_URL,
                                     query_string='offset={offset}&limit={limit}'.
                                     format(offset=0, limit=5))
        mock_query.assert_called_once()
        assert result.status == falcon.HTTP_OK

    @patch.object(Session, 'query')
    def test_GET_offset_and_limit_not_exist_failure(self, mock_query, client):
        """ 필수인 offset, limit 둘다 없을 때 """
        result = client.simulate_get(path=PUSH_CONTENTS_API_URL)
        assert result.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'query')
    def test_GET_limit_not_exist_failure(self, mock_query, client):
        """ limit 없을 때 """
        result = client.simulate_get(path=PUSH_CONTENTS_API_URL,
                                     query_string='offset={offset}'.
                                     format(offset=0))
        mock_query.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'query')
    def test_GET_offset_not_exist_failure(self, mock_query, client):
        """ offset 없을 때 """
        result = client.simulate_get(path=PUSH_CONTENTS_API_URL,
                                     query_string='limit={limit}'.
                                     format(limit=5))
        mock_query.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'commit')
    @patch.object(Session, 'add')
    def test_POST_success(self, mock_add, mock_commit, client):
        """ push content 등록 성공"""
        body = {'title': 'POST_API',
                'message': '10% Sales',
                'img_url': 'a.img',
                'url': 'where.to.go',
                'tag': 'general',
                'type': 'ordinary',
                }

        result = client.simulate_post(path=PUSH_CONTENTS_API_URL, json=body)

        mock_commit.assert_called_once()
        assert result.status == falcon.HTTP_CREATED

    @patch.object(Session, 'commit')
    @patch.object(Session, 'add')
    def test_POST_title_empty_failure(self, mock_add, mock_commit, client):
        """ Title 이 빈값으로 들어왔을때"""
        body = {'title': '',
                'message': '10% Sales',
                'img_url': 'a.img',
                'url': 'where.to.go',
                'tag': 'general',
                'type': 'ordinary',
                }
        result = client.simulate_post(path=PUSH_CONTENTS_API_URL, json=body)

        mock_add.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST