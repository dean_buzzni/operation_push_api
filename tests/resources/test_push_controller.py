from unittest.mock import patch
from datetime import datetime, timedelta

from pytz import timezone
import falcon

from sources.models.ops_push import OpsPush
from sources.models.ops_push_task import OpsPushTask
from sources.database import Session

# push 의 상태 전체 리스트
PUSH_STATE_LIST = {'created', 'tested', 'spawned', 'ready', 'sending',
                   'finished', 'failed', 'paused'}


@patch('sources.consumer.spawn.apply_async')
@patch.object(OpsPush, 'read_one')
class TestPushSpawn(object):
    API_URL = '/pushes/1/spawn'
    push_eta = datetime.now(tz=timezone('Asia/Seoul')) + timedelta(minutes=5)
    push_late_eta = datetime.now(tz=timezone('Asia/Seoul')) - timedelta(minutes=5)

    def test_POST_success(self, mock_read_one, mock_apply_async, client):
        mock_read_one.return_value = OpsPush(state='tested', eta=self.push_eta)
        response = client.simulate_post(self.API_URL)

        mock_apply_async.assert_called_once()
        assert response.status == falcon.HTTP_ACCEPTED

    def test_POST_not_allowed_state_failure(self, mock_read_one,
                                            mock_apply_async, client):
        """허용된 state 외에 모든 state 를 테스트 한다."""
        not_allow_state = PUSH_STATE_LIST.copy()
        not_allow_state.remove('tested')

        for state in not_allow_state:
            mock_read_one.return_value = OpsPush(state=state, eta=self.push_eta)
            response = client.simulate_post(self.API_URL)

        # task 는 한 번이라도 실행되면 안 된다.
        mock_apply_async.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST

    def test_POST_late_expire_failure(self, mock_read_one, mock_apply_async,
                                      client):
        """ eta 가 지났을 경우를 테스트 하낟."""
        mock_read_one.return_value = OpsPush(state='tested', eta=self.push_late_eta)
        response = client.simulate_post(self.API_URL)

        mock_apply_async.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST


@patch('sources.consumer.resume_push.apply_async')
@patch.object(OpsPush, 'read_one')
class TestPushResume(object):
    API_URL = '/pushes/1/resume'
    push_expire = datetime.now(tz=timezone('Asia/Seoul')) + timedelta(minutes=5)
    push_late_expire = datetime.now(tz=timezone('Asia/Seoul')) - timedelta(minutes=5)

    def test_POST_success(self, mock_read_one, mock_apply_async, client):
        mock_read_one.return_value = OpsPush(state='paused', expire=self.push_expire)
        response = client.simulate_post(self.API_URL)

        mock_apply_async.assert_called_once()
        assert response.status == falcon.HTTP_ACCEPTED

    def test_POST_not_allowed_state_failure(self, mock_read_one,
                                            mock_apply_async, client):
        """ 허용된 state 외에 모든 state 가 잘 실패되는지 확인한다."""
        not_allow_state = PUSH_STATE_LIST.copy()
        not_allow_state.remove('paused')

        for state in not_allow_state:
            mock_read_one.return_value = OpsPush(state=state, expire=self.push_expire)
            response = client.simulate_post(self.API_URL)

        # task 는 한 번이라도 실행되면 안 된다.
        mock_apply_async.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST

    def test_POST_late_expire_failure(self, mock_read_one, mock_apply_async,
                                      client):
        """ 시간이 지난 expire 였을때 잘 실해되는지 확인한다. """
        mock_read_one.return_value = OpsPush(state='paused', expire=self.push_late_expire)
        response = client.simulate_post(self.API_URL)

        mock_apply_async.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST


@patch('sources.consumer.test_push_send.apply_async')
@patch.object(OpsPush, 'read_one')
class TestPushTest(object):
    API_URL = '/pushes/1/test'
    ALLOW_STATE = {'created', 'tested', 'spawned'}

    def test_POST_success(self, mock_read_one, mock_apply_async, client):
        """ 허용된 모든 state 가 정상적으로 task 까지 실행시키는지 확인한다. """
        for state in self.ALLOW_STATE:
            mock_read_one.return_value = OpsPush(state=state)
            response = client.simulate_post(self.API_URL)

        assert mock_apply_async.call_count == len(self.ALLOW_STATE),\
            "{} 개의 state 가 성공하여야 하는데 {}개만 성공했습니다.".\
                format(len(self.ALLOW_STATE), mock_apply_async.call_count)
        assert response.status == falcon.HTTP_ACCEPTED

    def test_POST_not_allowed_state_failure(self, mock_read_one,
                                            mock_apply_async, client):
        """ 허용된 state 외에 모든 state 가 실패처리 되는지 확인한다. """
        not_allow_state = PUSH_STATE_LIST - self.ALLOW_STATE

        for state in not_allow_state:
            mock_read_one.return_value = OpsPush(state=state)
            response = client.simulate_post(self.API_URL)

        mock_apply_async.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST


@patch('sources.consumer.app.control.revoke')
@patch.object(Session, 'commit')
@patch.object(OpsPush, 'read_one')
@patch.object(Session, 'query')
class TestPushStop(object):
    API_URL = '/pushes/1/stop'
    # ops_push_task 까지 컨트롤 하는 state
    ALLOW_STATE = {'ready', 'sending'}
    # ops_push 의 state 만 바꿔주면 되는 state
    ALLOW_STATE_2 = {'created', 'tested', 'spawned'}

    def test_POST_with_ops_task_success(self, mock_query, mock_read_one,
                                        mock_commit, mock_revoke, client):
        """ ops_push_task 까지 처리해야 하는 state 들이 revoke 까지 정상적으로 하는지 확인한다. """
        for state in self.ALLOW_STATE:
            mock_read_one.return_value = OpsPush(state=state)
            mock_query().filter().filter().all.\
                return_value = [OpsPushTask(task_uuid='1234')]

            response = client.simulate_post(self.API_URL)

        assert mock_revoke.call_count == len(self.ALLOW_STATE), \
            "{} 개의 시도 모두 revoke 까지 실행되어야 하는데 {}개만 성공했습니다.". \
                format(len(self.ALLOW_STATE), mock_revoke.call_count)
        assert response.status == falcon.HTTP_OK

    def test_POST_success(self, mock_query, mock_read_one, mock_commit,
                          mock_revoke, client):
        """ ops_push 까지만 처리해주면 되는 state 들이 정상적으로 성공하는지 확인한다.

        해당 부분의 revoke 할 ops_push_task 가 없음으로 mock_revoke 가 실행되지 않는지도
        확인한다.
        """
        for state in self.ALLOW_STATE_2:
            mock_read_one.return_value = OpsPush(state=state)
            response = client.simulate_post(self.API_URL)

        assert mock_commit.call_count == len(self.ALLOW_STATE_2), \
            "{} 개의 시도 모두 commit 까지 실행되어야 하는데 {}개만 성공했습니다.". \
                format(len(self.ALLOW_STATE_2), mock_commit.call_count)
        mock_revoke.assert_not_called()
        assert response.status == falcon.HTTP_OK

    def test_POST_with_ops_task_empty_task_failure(self, mock_query,
                                                   mock_read_one, mock_commit,
                                                   mock_revoke, client):
        """ ops_push_task 까지 처리해야 하는데 조건에 부합하는 하위 task 가 없을 경우 """
        mock_read_one.return_value = OpsPush(state=list(self.ALLOW_STATE)[0])
        mock_query().filter(). \
            filter().all.return_value = []

        response = client.simulate_post(self.API_URL)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_INTERNAL_SERVER_ERROR

    def test_POST_not_allowed_state_failure(self, mock_query, mock_read_one,
                                            mock_commit, mock_revoke, client):
        """ 허용된 state 외에 모든 state 가 정상적으로 실패처리 되는지 확인한다."""
        not_allow_state = PUSH_STATE_LIST - self.ALLOW_STATE - self.ALLOW_STATE_2

        for state in not_allow_state:
            mock_read_one.return_value = OpsPush(state=state)
            response = client.simulate_post(self.API_URL)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST


@patch('sources.consumer.app.control.revoke')
@patch.object(Session, 'commit')
@patch.object(OpsPush, 'read_one')
@patch.object(Session, 'query')
class TestPushPause(object):
    API_URL = '/pushes/1/pause'

    def test_POST_success(self, mock_query, mock_read_one,
                          mock_commit, mock_revoke, client):
        mock_read_one.return_value = OpsPush(state='sending')
        mock_query().filter().filter().all.\
            return_value = [OpsPushTask(task_uuid='1234')]

        response = client.simulate_post(self.API_URL)

        mock_revoke.assert_called_once()
        assert response.status == falcon.HTTP_OK

    def test_POST_empty_task_failure(self, mock_query, mock_read_one,
                                     mock_commit, mock_revoke, client):
        """ ops_push_task 까지 처리해야 하는데 조건에 부합하는 하위 task 가 없을 경우 """
        mock_read_one.return_value = OpsPush(state='sending')
        mock_query().filter().filter().all.return_value = []

        response = client.simulate_post(self.API_URL)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_INTERNAL_SERVER_ERROR

    def test_POST_not_allowed_state_failure(self, mock_query, mock_read_one,
                                            mock_commit, mock_revoke, client):
        """ 허용된 state 외에 모든 state 가 정상적으로 실패처리 되는지 확인한다."""
        not_allow_state = PUSH_STATE_LIST.copy()
        not_allow_state.remove('sending')

        for state in not_allow_state:
            mock_read_one.return_value = OpsPush(state=state)
            response = client.simulate_post(self.API_URL)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST