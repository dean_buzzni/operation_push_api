import pickle
from unittest.mock import patch

import falcon
from sqlalchemy.orm.exc import NoResultFound

from sources.models.ops_push import OpsPush
from sources.database import Session, redis_session

PUSH_API_URL = '/pushes'


class TestPush(object):

    @patch.object(pickle, "loads")
    @patch.object(redis_session, 'get')
    @patch.object(OpsPush, 'read_one')
    def test_GET_success(self, mock_read_one, mock_redis_session_get,
                         mock_pickle_loads, client):
        """정상적인 요청을 했을때"""
        valid_input = {
            'ops_push_content_id':1,
            'ops_push_user_extraction_id':1,
            'eta':"20180704171530",
            'expire':"20180704171530"
        }
        mock_pickle_loads.side_effect = \
            [{'count': 23}, [1, 2, 3]]

        mock_read_one.return_value = OpsPush(**valid_input)
        response = client.simulate_get(PUSH_API_URL + '/1')

        assert response.status == falcon.HTTP_OK

    @patch.object(OpsPush, 'read_one')
    def test_GET_not_found_failure(self, mock_read_one, client):
        """ 존재하지 않는 id 로 요청했을때"""
        mock_read_one.side_effect = NoResultFound()
        response = client.simulate_get(PUSH_API_URL + '/2')

        assert response.status == falcon.HTTP_NOT_FOUND


@patch.object(Session, 'query')
class TestPushCollection(object):

    def test_GET_success(self, mock_query, client):
        """push list 가져오기 성공 테스트"""
        response = client.simulate_get(path=PUSH_API_URL,
                                       query_string='offset={offset}&limit={limit}'.
                                       format(offset=0, limit=5))
        assert response.status == falcon.HTTP_OK

    def test_GET_offset_and_limit_not_exist_failure(self, mock_query, client):
        """ 필수 값인 offset 과 limit 이 없을때 """
        response = client.simulate_get(PUSH_API_URL)
        assert response.status == falcon.HTTP_BAD_REQUEST, "{} 가 왔습니다.".format(response.status)

    @patch.object(redis_session, 'set')
    @patch.object(Session, 'commit')
    def test_POST_success(self, mock_commit, mock_redis_session_set,
                          mock_query, client):
        """ Push 생성 성공 테스트"""
        body = {
            "eta": "20180809123100",
            "expire": "20180809123600",
            "ops_push_content_id": 1,
            "ops_push_user_extraction_id": 1,
            "push_id_to_deny": []
        }

        mock_query().scalar.return_value = False
        mock_query().filter().one.return_value = [5]

        response = client.simulate_post(PUSH_API_URL, json=body)

        mock_commit.assert_called_once()
        assert response.status == falcon.HTTP_CREATED

    @patch.object(Session, 'commit')
    def test_POST_short_excution_time_failure(self, mock_commit, mock_query,
                                              client):
        """ETA~Expire 시간이 너무 짧을때"""
        body = {
            "eta": "20180809123100",
            "expire": "20180809123300",
            "ops_push_content_id": 1,
            "ops_push_user_extraction_id": 1,
            "push_id_to_deny": []
        }

        mock_query().scalar.return_value = False
        mock_query().filter().one.return_value = [26000]

        response = client.simulate_post(PUSH_API_URL, json=body)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'commit')
    def test_POST_schedule_conflict_failure(self, mock_commit, mock_query,
                                            client):
        """schedule 이 겹칠 경우"""
        body = {
            "eta": "20180809123100",
            "expire": "20180809123600",
            "ops_push_content_id": 1,
            "ops_push_user_extraction_id": 1,
            "push_id_to_deny": []
        }

        mock_query().scalar.return_value = True
        response = client.simulate_post(PUSH_API_URL, json=body)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_CONFLICT

    @patch.object(Session, 'commit')
    def test_POST_eta_not_exist_failure(self, mock_commit, mock_query, client):
        """필수 값이 입력되지 않은 경우"""
        body = {
            "expire": "20180809123600",
            "ops_push_content_id": 1,
            "ops_push_user_extraction_id": 1,
            "push_id_to_deny": []
        }

        response = client.simulate_post(PUSH_API_URL, json=body)

        mock_commit.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST
