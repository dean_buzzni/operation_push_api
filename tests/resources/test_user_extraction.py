import json
from unittest.mock import patch

import falcon

from sources.models.ops_push import OpsPush
from sources.models.ops_push_user_extraction import OpsPushUserExtraction
from sources.database import Session

USER_EXTRACTION_URL = '/userextractions'


class TestUserExtraction(object):
    """단수 push_user_extraction 에 대한 테스트 묶음"""

    @patch.object(OpsPushUserExtraction, 'read_one')
    def test_GET_success(self, mock_read_one, client):
        """ user_extraction 가져오기 성공 테스트"""
        valid_input = {
            "os": "android",
            "day_interval": -60,
            "type": "ordinary",
            "date_offset": "20180704171530"
        }

        mock_read_one.return_value = OpsPushUserExtraction(**valid_input)

        response = client.simulate_get(USER_EXTRACTION_URL + '/1')

        response_dict = json.loads(response.text)

        assert response.status == falcon.HTTP_OK
        assert response_dict['os'] == valid_input['os']

    @patch.object(OpsPush, 'is_exists')
    @patch.object(Session, 'delete')
    @patch.object(Session, 'commit')
    @patch.object(OpsPushUserExtraction, 'read_one')
    def test_DELETE_success(self, mock_read_one, mock_commit, mock_delete,
                            mock_exists, client):
        """ user_extraction 삭제 성공 테스트 """
        mock_exists.return_value = False
        response = client.simulate_delete(USER_EXTRACTION_URL + '/1')

        mock_commit.assert_called_once()
        assert response.status == falcon.HTTP_NO_CONTENT


class TestUserExtractionCollection(object):

    @patch.object(Session, 'commit')
    @patch.object(Session, 'add')
    @patch.object(Session, 'close')
    def test_POST_success(self, mock_close, mock_add, mock_commit, client):
        """ user_extraction 생성 성공 테스트 """
        valid_input = {
            "os": "android",
            "day_interval": -60,
            "type": "ordinary"
        }
        response = client.simulate_post(USER_EXTRACTION_URL,
                                        json=valid_input)

        mock_commit.assert_called_once()
        mock_close.assert_called_once()
        assert response.status == falcon.HTTP_CREATED

    @patch.object(Session, 'commit')
    @patch.object(Session, 'add')
    def test_POST_wrong_os_failure(self, mock_add, mock_commit, client):
        """ 없는 os 를 입력했을경우 실패 테스트 os 는 ('ios' ,'android') 만 있다."""
        valid_input = {
            "os": "androidadq",
            "day_interval": -60,
            "type": "ordinary"
        }

        response = client.simulate_post(USER_EXTRACTION_URL,
                                        json=valid_input)

        mock_add.assert_not_called()
        assert response.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'close')
    @patch.object(Session, 'query')
    def test_GET_success(self, mock_query, mock_close, client):
        """get API는 query string으로 반드시 offset, limit을 받아야함"""
        result = client.simulate_get(path=USER_EXTRACTION_URL,
                                     query_string='offset={offset}&limit={limit}'.
                                     format(offset=0, limit=5))
        mock_query.assert_called_once()
        mock_close.assert_called_once()
        assert result.status == falcon.HTTP_OK

    @patch.object(Session, 'query')
    def test_GET_offset_and_limit_not_exist_failure(self, mock_query, client):
        """ 필수 값인 offset, limit 둘다 없을 때 """
        result = client.simulate_get(path=USER_EXTRACTION_URL)
        mock_query.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'query')
    def test_GET_limit_not_exist_failure(self, mock_query, client):
        """ limit 이 없을때 """
        result = client.simulate_get(path=USER_EXTRACTION_URL,
                                     query_string='offset={offset}'.
                                     format(offset=0))
        mock_query.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST

    @patch.object(Session, 'query')
    def test_GET_offset_not_exist_failure(self, mock_query, client):
        """ offset 없을 때 """
        result = client.simulate_get(path=USER_EXTRACTION_URL,
                                     query_string='limit={limit}'.
                                     format(limit=5))
        mock_query.assert_not_called()
        assert result.status == falcon.HTTP_BAD_REQUEST