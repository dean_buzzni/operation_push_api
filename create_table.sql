CREATE TYPE push_content_type AS ENUM ('ordinary', 'moaweek', 'search_personalize');
CREATE TABLE IF NOT EXISTS OPS_PUSH_CONTENT (
  id  BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- contents
  title TEXT NOT NULL,
  message TEXT,
  img_url TEXT,
  url TEXT,
  tag TEXT,
  is_ad BOOLEAN,
  sound TEXT,
  is_popup BOOLEAN,
  type push_content_type NOT NULL
);
CREATE TABLE IF NOT EXISTS OPS_PUSH_USER_EXTRACTION (
  id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- contents
  os TEXT NOT NULL,
  extract_count INTEGER NOT NULL,
  type TEXT NOT NULL,
  value TEXT,
  date_offset TIMESTAMP with time zone,
  day_interval INTERVAL NOT NULL
);
CREATE TABLE IF NOT EXISTS OPS_PUSH(
  id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- Foreign KEY --
  ops_push_content_id BIGINT references ops_push_content(id),
  ops_push_user_extraction_id BIGINT references ops_push_user_extraction(id),
  -- contents
  eta TIMESTAMP with time zone,
  expire TIMESTAMP with time zone,
  -- sytemical columns
  task_uuid TEXT,
  state TEXT
);
CREATE TABLE IF NOT EXISTS OPS_PUSH_TASK(
  id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- Foreign KEY --
  ops_push_id BIGINT references ops_push(id),
  -- sytemical columns
  task_uuid TEXT NOT NULL,
  state TEXT
);
CREATE TABLE IF NOT EXISTS OPS_PUSH_RECEIVER(
  id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- Foreign KEY --
  ops_push_id BIGSERIAL references ops_push(id),
  ops_push_task_id BIGSERIAL references ops_push_task(id),
  -- contents
  user_id TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS OPS_PUSH_TEST_RECEIVER(
  id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP with time zone NOT NULL,
  last_modified TIMESTAMP with time zone NOT NULL,
  -- content
  user_id TEXT NOT NULL,
  tag TEXT
);
